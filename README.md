# NHR

## Description
NHR (NHR is a Handwriting Recognizer) is a Real-time Devanagari Handwriting Recognizer.

## Dependencies
NHR requires installation of Java Development Kit.

### For debain based distros

    apt-get install openjdk-7-jdk

### For arch linux

    pacman -S jdk7-openjdk

## Compiling and Running
Check out the latest sources with:

    git clone https://gitlab.com/nprs/nhr

To build:

    ./gradlew gui:build

Run the program:

    ./nhr

## Usage
    ./nhr [-f <arg>] [-m <arg>] [-M <arg>] [-t]
      -f,--file <arg>    Data file
      -m,--mode <arg>    Mode of operation [train|recognize|view|learn]
      -M,--model <arg>   Learning model to use [SVM|KNN|ANN]
      -t,--testonly      Test only, don't take input

The Devanagari script consists of upper modifiers and lower modifiers. So, recognition additionally requires both upper modifiers and lower modifiers data to work properly.
In recognize mode, the program searches for three file in data/ directory: `FILE.lower`, `FILE.middle`, `FILE.lower`.
In other modes, the program searches for `FILE` in data/ directory. The upper and lower modifiers must be trained separately.
