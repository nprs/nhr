## Data representation `ONTIME`
* How is the stroke represented? `DONE`
    Time-series collection of points in two dimension. Ex
    `ArrayList<Point>`
* How is the stroke stored? `DONE`
    Stored in plain text. Ex:
    `label; x1,y1 x2,y2 ;x3,y3 x4,y4 ;`
* Can strokes be connected which are very in temporal space? `PROPOSAL`
    Yes or No

## Data collection `TODO` `IMPORTANT` `LATE`
* Need handwriting data from wide range of users.
    Currently 7 writers. DTW is writer dependent.
* Need a widely used medium to collect data. `NOT NEEDED`
    Android devices. Working on java which can be ported to android systems.
* Users can write whole paragraph, sentence, word or character?
    Character
* Users can write in tilted fashion?
    No

## Segmentation `NOT NEEDED`
* Apply contiguity based clustering, with horizontal distance between centroids. `NOT NEEDED`
* Apply separation according to bounding box information dynamically. `DONE` `ALTERNATIVE`

## Pre-processing
* Preview of pre-processing. `DONE`
* Remove noise points. `TODO`
* Join abrupt penup-pendown. `TODO`
* Identify bounding boxes. `DONE`
* Identify linear strokes and dots using Circular statistics and dispersion. `DONE`
* Identify shirorekha. `DONE`
* Identify modifiers using location and boundary box information. `DONE`
* Identify upper, middle and lower zones using bounding-box info and shirorekha. `DONE` `IMPORTANT`
* Fix for ka, fa by splitting vertically. `DONE`
* Fix for ki,ke by splitting horizontally. `DONE`
* Remove | for characters. `DONE`
* Sorting according to bounding box information. `DONE`
* Unification. `DONE`
* Smoothing using Double-sided Moving Average `DONE`
* Resampling using Linear Interpolation `DONE`
* Normalization preserving aspect ratio `DONE`
* Modify zoning algorithm and sorting algorithm. `DONE`

## Recognition
* Dynamic Time Warping as distance metric using Cosine Similarity `DONE`
* k-nearest neighbor classifier `DONE`
* Fast Fourier Transform `DONE`
* Another approach: Support Vector Machine, Neural Network `DONE`
* Add numerals, symbols? `TODO`
* Recognition of ":" `TODO`

## Post-processing
* Identify adha ka/fa by using split ratio
* Identify half characters and modifiers by checking presence of |. `DONE`
* Identify modifier / using linearity information. `DONE`
* Create language model: Nepali Dictionary using hunspell `TODO`

## Testing `TODO`
* Visulalize stroke data. `DONE`
* Visualize the TRP, TNR for the test condition in given dataset. `TODO` `IMPORTANT`
  Separate according to number of strokes and labels after processing, then cluster using DTW to get different types for each label.
  Use at least one example for each type for all label.
* List empirical constants, and identify best. `TODO` `IMPROTANT`
* Get optimal value of K for KNN. `TODO`

## Limitation
* The recognition depends heavily on the identification of linear lines,
both horizontal and vertical.
* Cursive mixed modifiers aren't fully supported.
* Identification of zones is performed using the size of the tallest or vertical stroke.
* Simple Segmentation of characters is not implemented.

# Details
* multi-small contains all the composite characters
* Remove halantas from unismall and merge into uni-big
* Unibig becomes the overloaded.middle

# Fix Required Immediately
* Create different label for different stroke type in same label as well, sorting is done `IN PROGRESS`
* Unmerged files are in overloaded.log and overloaded.misclassified
* Add recognize in viewmode
* Add view in recognizemode
* Add half "ka", "fa"
* Internal segmentation problem in "ha", "cha"
* Unwanted splits in "ra", "cha", "da"
