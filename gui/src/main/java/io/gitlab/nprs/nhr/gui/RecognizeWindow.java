package io.gitlab.nprs.nhr.gui;

import java.util.*;
import java.io.File;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import io.gitlab.nprs.nhr.engine.Stroke;
import io.gitlab.nprs.nhr.engine.Graph;
import io.gitlab.nprs.nhr.engine.Bag;
import io.gitlab.nprs.nhr.engine.Segmentator;
import io.gitlab.nprs.nhr.engine.Brain;
import io.gitlab.nprs.nhr.engine.Tuple;
import io.gitlab.nprs.nhr.engine.TestReport;
import io.gitlab.nprs.nhr.engine.ListEmptyError;
import io.gitlab.nprs.nhr.engine.LabeledStroke;
import io.gitlab.nprs.nhr.engine.LearningModel;
import io.gitlab.nprs.nhr.engine.KNN;
import io.gitlab.nprs.nhr.engine.ANNEnc;
import io.gitlab.nprs.nhr.engine.SVMEnc;
import io.gitlab.nprs.nhr.engine.DTW;
import io.gitlab.nprs.nhr.engine.ModelsFile;
import io.gitlab.nprs.nhr.engine.ListEmptyError;
import io.gitlab.nprs.nhr.engine.Options.Model;

public class RecognizeWindow extends Window {

    private Bag misclassifiedBag;
    private Bag logBag;
    private LearningModel<LabeledStroke> upperClassifier;
    private LearningModel<LabeledStroke> middleClassifier;
    private LearningModel<LabeledStroke> lowerClassifier;
    // store the last predicted label, used to store
    // it if classification is wrong

    private List<Graph> inputGraphs;

    private String recognize() {
        try {
            String predictedChar = "";
            canvas.clearScreen();

            inputGraphs = Segmentator.segment(canvas.strokes(), null);

            for (Graph graph: inputGraphs) {
                List<String> suggestionHolder = new ArrayList<String>();
                // try to predict from the canvas strokes and draw the debugging
                // information on canvas
                List<Stroke> strokes = Brain.workFor(graph, upperClassifier,
                        middleClassifier, lowerClassifier, suggestionHolder);
                // predict the class
                predictedChar += graph.label();

                canvas.drawStrokes(strokes);
            }
            canvas.repaint();
            canvas.garbage = true;

            // Store recognized graphs
            for (Graph graph: inputGraphs) {
                logBag.append(graph);
                logBag.store();
            }

            return predictedChar+" ";

        } catch (ListEmptyError ls) {
            System.out.println(ls.msg);
            System.out.println("RecognizeWindow.recognize(): Nothing has been drawn.");
            return "";
        }
    }

    public RecognizeWindow(String filename, Model model, boolean testOnly) {
        // true signifies drawable
        super(true);

        // create recognize button
        JButton recognizeButton = new JButton("Recognize");
        recognizeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String label = textfield.getText();
                label += recognize();
                textfield.setText(label);
            }
        });
        panel.add(recognizeButton);

        // create misclassify button
        JButton misclassifyButton = new JButton("Misclassify");
        misclassifyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (Graph graph: inputGraphs) {
                    misclassifiedBag.append(graph);
                    misclassifiedBag.store();
                    logBag.remove(graph);
                    logBag.store();
                }
            }
        });
        panel.add(misclassifyButton);

        // TODO refactor the FAQ out of this later
        String modelFName = filename+"."+model.name().toLowerCase()+".mdl";
        File modelFile = new File(modelFName);
        if (modelFile.exists() && !modelFile.isDirectory()) {
            ModelsFile mf = new ModelsFile(modelFName,model);
            try {
                mf.readModels();
                upperClassifier = mf.upper;
                middleClassifier = mf.middle;
                lowerClassifier = mf.lower;
                if (model==Model.SVM) {
                    // See if there's an ANN model too. If there is, it can be used
                    // for providing suggestions.
                    File annFile = new File(filename+".ann.mdl");
                    if (annFile.exists() && !annFile.isDirectory()) {
                        ModelsFile annMdl = new ModelsFile(filename+".ann.mdl",Model.ANN);
                        annMdl.readModels();
                        ((SVMEnc<LabeledStroke>)upperClassifier).setANN(
                            (ANNEnc<LabeledStroke>)annMdl.upper);
                        ((SVMEnc<LabeledStroke>)middleClassifier).setANN(
                            (ANNEnc<LabeledStroke>)annMdl.middle);
                        ((SVMEnc<LabeledStroke>)lowerClassifier).setANN(
                            (ANNEnc<LabeledStroke>)annMdl.lower);
                    }
                }
            } catch (Exception ex) {
                System.out.println("Exception in readModels(): "+ex.getMessage());
                System.exit(1);
            }
        } else {
            System.out.println("RecognizeWindow(): No such model file!");

            Bag middleBag = Bag.load(filename+".middle");
            Bag upperBag = Bag.load(filename+".upper");
            Bag lowerBag = Bag.load(filename+".lower");

            // Initialize model classifiers for each yard: upper, middle. lower
            // only one model now
            if (model==Model.KNN) {
                middleClassifier = new KNN<LabeledStroke>(
                        (int)Math.sqrt(middleBag.graphs().size()),
                        new DTW<LabeledStroke>());
                lowerClassifier = new KNN<LabeledStroke>(
                        (int)Math.sqrt(lowerBag.graphs().size()),
                        new DTW<LabeledStroke>());
                upperClassifier = new KNN<LabeledStroke>(
                        (int)Math.sqrt(upperBag.graphs().size()),
                        new DTW<LabeledStroke>());
                System.out.println("Model KNN");
            } else if (model==Model.SVM) {
                System.out.println("Model SVM");
                middleClassifier = new SVMEnc<LabeledStroke>();
                lowerClassifier = new SVMEnc<LabeledStroke>();
                upperClassifier = new SVMEnc<LabeledStroke>();
            } else {
                System.out.println("Model ANN");
                middleClassifier = new ANNEnc<LabeledStroke>();
                lowerClassifier = new ANNEnc<LabeledStroke>();
                upperClassifier = new ANNEnc<LabeledStroke>();
            }
            Brain.learn(middleBag,middleClassifier,true);
            Brain.learn(lowerBag,lowerClassifier,false);
            Brain.learn(upperBag,upperClassifier,false);
        }
        logBag = Bag.load(filename+".log");
        misclassifiedBag = Bag.load(filename+".missclassified");

        inputGraphs = new ArrayList<Graph>();
    }

}
