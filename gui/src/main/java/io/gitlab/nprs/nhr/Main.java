package io.gitlab.nprs.nhr;

import javax.swing.JFrame;

import io.gitlab.nprs.nhr.gui.Window;
import io.gitlab.nprs.nhr.gui.RecognizeWindow;
import io.gitlab.nprs.nhr.gui.TrainWindow;
import io.gitlab.nprs.nhr.gui.ViewWindow;
import io.gitlab.nprs.nhr.engine.Options;
import io.gitlab.nprs.nhr.engine.Bag;
import io.gitlab.nprs.nhr.engine.Brain;
import io.gitlab.nprs.nhr.engine.LearningModel;
import io.gitlab.nprs.nhr.engine.LabeledStroke;
import io.gitlab.nprs.nhr.engine.ModelsFile;
import io.gitlab.nprs.nhr.engine.Options.Mode;
import io.gitlab.nprs.nhr.engine.Options.Model;

public class Main {

    // main function that is invoked
    public static void main(String[] args) {
        Options options = new Options(args);
        if (options.argsDeformed) {
            options.printHelp();
            return;
        }

        // Create main window
        if (options.mode==Mode.LEARN) {
            try {
                System.out.println("LEARNING MODE IS GO");
                Bag middleBag = Bag.load(options.dataFile+".middle");
                Bag upperBag = Bag.load(options.dataFile+".upper");
                Bag lowerBag = Bag.load(options.dataFile+".lower");
                String modelFileName = options.dataFile+"."+
                    options.model.name().toLowerCase()+".mdl";
                ModelsFile mf = new ModelsFile(modelFileName,options.model);
                mf.instantiateAll();
                LearningModel<LabeledStroke> upperClassifier = mf.upper;
                LearningModel<LabeledStroke> lowerClassifier = mf.lower;
                LearningModel<LabeledStroke> middleClassifier = mf.middle;
                Brain.learn(middleBag,mf.middle,true);
                Brain.learn(lowerBag,mf.lower,false);
                Brain.learn(upperBag,mf.upper,false);
                mf.writeModels();
                System.out.println("Wrote "+modelFileName);
            } catch (Exception ex) {
                System.out.println("Exception! "+ex.getMessage());
                return;
            }
        } else if (options.mode==Mode.VALIDATE) {
            // k for k-fold cross validation
            int k = 5;
        } else {
            Window frame;
            if (options.mode==Mode.RECOGNIZE) {
                frame = new RecognizeWindow(options.dataFile,options.model,options.testOnly);
            } else if (options.mode==Mode.TRAIN) {
                frame = new TrainWindow(options.dataFile);
            } else if (options.mode==Mode.VIEW) {
                frame = new ViewWindow(options.dataFile);
            } else return;
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        }
    }
}
