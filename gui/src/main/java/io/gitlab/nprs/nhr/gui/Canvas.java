package io.gitlab.nprs.nhr.gui;

import java.util.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseEvent;

import java.awt.Color;
import java.awt.BasicStroke;
import java.awt.RenderingHints;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JComponent;

import io.gitlab.nprs.nhr.engine.Point;
import io.gitlab.nprs.nhr.engine.Stroke;
import io.gitlab.nprs.nhr.engine.Segmentator;
import io.gitlab.nprs.nhr.engine.BoundingBox;
import io.gitlab.nprs.nhr.engine.ListEmptyError;

public class Canvas extends JComponent {
    private Image image;
    private Graphics2D graphics2D;

    // specifies alternate color for strokes
    private boolean alternate;
    // specifies if user is dragging or not
    private boolean dragged;
    // specifies if content in screen is garbage, and needs to be cleared
    public boolean garbage;
    // specifies if user can draw on the canvas
    private boolean drawable;

    // stores last point in current drawing stroke
    private Point old;
    // stores the current drawing stroke
    private List<Point> activePoints;
    // stores the total strokes in canvas
    private List<Stroke> activeStrokes;

    // Initializer block
    {
        // NOTE: Not really required, initialized at mouse pressed anyway
        old = new Point(0, 0);

        activePoints = new ArrayList<Point>();
        activeStrokes = new ArrayList<Stroke>();
    }

    public Canvas(boolean drawable) {
        this.drawable = drawable;
        if (!drawable)
            return;

        addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                // see is older garbage exists
                if (garbage) {
                    garbage = false;
                    clear();
                }

                Point current = new Point(e.getX(), e.getY());

                // Draw starting point
                graphics2D.setColor(Color.red);
                graphics2D.fillOval(current.x-4, current.y-4, 8, 8);
                repaint();

                old = current;

                activePoints.add(current);

                alternate = false;
                dragged = true;


                graphics2D.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT,
                            BasicStroke.JOIN_BEVEL));

            }

            public void mouseReleased(MouseEvent e) {
                // if mouse is released, add active stroke to list of strokes
                // and create new active stroke

                try {
                    Stroke stroke = new Stroke(activePoints);
                    stroke.isNear(0, 20);
                    activeStrokes.add(stroke);
                } catch (ListEmptyError ls) {
                    System.out.println(ls.msg);
                }

                activePoints = new ArrayList<Point>();
                dragged = false;

                clearScreen();

                drawBoxes(activeStrokes);
                drawStrokes(activeStrokes);
                repaint();
            }

        });

        addMouseMotionListener(new MouseMotionAdapter() {
            // if the mouse is being dragged, add points to active stroke and
            // draw line joining the points
            public void mouseDragged(MouseEvent e) {
                if (!dragged)
                    return;

                Point current = new Point(e.getX(), e.getY());
                // no need to do extra work if current point is equal to old point
                if (current.x == old.x && current.y == old.y)
                    return;

                // Draw line
                graphics2D.setColor(getColor());
                graphics2D.drawLine(old.x, old.y, current.x, current.y);
                repaint();

                // Add the new point to active points
                activePoints.add(current);
                old = current;
            }
        });
    }

    // get strokes
    public List<Stroke> strokes() {
        return activeStrokes;
    }

    // set strokes
    public void strokes(List<Stroke> strokes) {
        // NOTE: strokes isn't cloned
        activeStrokes = strokes;
    }

    // Remove the last drawn stroke
    public void undo() {
        // there is nothing to remove
        if (activeStrokes.isEmpty())
            return;

        // no garbage because undo action imples that user wants to edit
        garbage = false;
        activeStrokes.remove(activeStrokes.size()-1);

        // Display the changes in screen
        clearScreen();
        drawBoxes(activeStrokes);
        drawStrokes(activeStrokes);
        repaint();
    }

    // Remove all the strokes
    public void clear() {
        // no garbage because everything is cleared
        garbage = false;
        activeStrokes = new ArrayList<Stroke>();

        // Display the change in screen
        clearScreen();
        repaint();
    }

    // Clear the screen with white paint
    public void clearScreen() {
        graphics2D.setPaint(Color.white);
        graphics2D.fillRect(0, 0, getSize().width, getSize().height);
    }

    // Set alternate colors for drawing strokes (helper function)
    private Color getColor() {
        alternate = !alternate;
        return alternate?Color.black:Color.lightGray;
    }

    public void drawStroke(Stroke stroke) {
        drawStroke(stroke, null);
    }

    public void drawStroke(Stroke stroke, Color color) {
        if (color==null)
            color = Color.darkGray;

        graphics2D.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT,
                    BasicStroke.JOIN_BEVEL));

        boolean start = true;
        alternate = false;
        Point prev = new Point(0, 0);
        for (Point p : stroke.points()){
            if (start) {
                // Draw circle at start of stroke
                start = false;
                graphics2D.setColor(Color.red);
                graphics2D.fillOval(p.x-4, p.y-4, 8, 8);
            } else {
                graphics2D.setColor(color);
                graphics2D.drawLine(prev.x, prev.y, p.x, p.y);
            }
            prev = p;
        }
    }

    public void Xaxis(int offset, Color color) {
        graphics2D.setColor(color);
        graphics2D.drawLine(0, offset, 800, offset);
    }

    public void Yaxis(int offset, Color color) {
        graphics2D.setColor(color);
        graphics2D.drawLine(offset, 0, offset, 600);
    }

    public void drawStrokes(List<Stroke> strokes) {
        drawStrokes(strokes, null);
    }

    public void drawBox(BoundingBox bbox, int padding) {

        graphics2D.setColor(Color.green);
        graphics2D.setStroke(new BasicStroke(1, BasicStroke.CAP_BUTT,
                             BasicStroke.JOIN_BEVEL));
        graphics2D.fillOval(bbox.center.x-4, bbox.center.y-4, 8, 8);
        graphics2D.drawRect(bbox.topLeft.x-padding,
                            bbox.topLeft.y-padding,
                            bbox.width()+2*padding,
                            bbox.height()+2*padding);
    }

    public void drawBoxes(List<Stroke> strokes) {
        try {
            List<BoundingBox> boxlist = new ArrayList<BoundingBox>();
            // no need to get return values, just for populating bounding boxes
            Segmentator.segment(strokes, boxlist);

            for (BoundingBox bbox: boxlist)
                drawBox(bbox, Segmentator.hSpace);

        } catch (ListEmptyError ls) {
            System.out.println(ls.msg);
            // shouldn't happen
        }
    }

    // Redraw screen using stokes saved in memory
    public void drawStrokes(List<Stroke> strokes, Color color) {
        for (Stroke stroke : strokes) {
            drawStroke(stroke, color);
        }
    }

    // Draws the image
    public void paintComponent(Graphics g) {
        if (image == null) {
            // NOTE: Only runs at start
            image = createImage(getSize().width, getSize().height);
            graphics2D = (Graphics2D) image.getGraphics();
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            clearScreen();
            repaint();
        }
        g.drawImage(image, 0, 0, null);
    }
}
