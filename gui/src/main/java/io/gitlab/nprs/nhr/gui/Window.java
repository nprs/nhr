package io.gitlab.nprs.nhr.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Dimension;
import java.awt.Container;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Window extends JFrame {
    protected JPanel panel;
    public Canvas canvas;
    protected JTextField textfield;

    public Window(boolean drawable) {
        setTitle("nhr");
        setSize(800, 600);
        setResizable(false);
        setLocationRelativeTo(null);

        // Get container from window
        Container content = this.getContentPane();
        content.setLayout(new BorderLayout());

        canvas = new Canvas(drawable);
        content.add(canvas, BorderLayout.CENTER);

        // Create a panel for button and textfield
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        content.add(panel, BorderLayout.NORTH);

        // Create a textfield
        textfield = new JTextField();
        textfield.setEditable(true);
        textfield.setMaximumSize(new Dimension(Integer.MAX_VALUE,
                    textfield.getPreferredSize().height));
        panel.add(textfield);

        // if drawable
        if (drawable) {
            // create undo button
            JButton undoButton = new JButton("Undo");
            undoButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    canvas.undo();
                }
            });
            panel.add(undoButton);

            // create clear button
            JButton clearButton = new JButton("Clear");
            clearButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    canvas.clear();
                }
            });
            panel.add(clearButton);
        }
    }
}
