package io.gitlab.nprs.nhr.gui;

import java.util.*;

import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import javax.swing.KeyStroke;
import javax.swing.AbstractAction;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;

import javax.swing.JButton;

import io.gitlab.nprs.nhr.engine.Tuple;
import io.gitlab.nprs.nhr.engine.Stroke;
import io.gitlab.nprs.nhr.engine.Graph;
import io.gitlab.nprs.nhr.engine.Bag;
import io.gitlab.nprs.nhr.engine.Brain;
import io.gitlab.nprs.nhr.engine.ListEmptyError;
import io.gitlab.nprs.nhr.engine.Segmentator;
import io.gitlab.nprs.nhr.engine.BoundingBox;
import io.gitlab.nprs.nhr.engine.Point;

public class ViewWindow extends Window {

    Bag bag;

    // for view mode only
    private int viewindex;

    private void test() {

        canvas.clearScreen();
        Graph g = bag.graphs().get(viewindex);
        textfield.setText(g.label());
        test1(g);
        canvas.repaint();
        test2(g);
        canvas.repaint();
    }

    private void test1(Graph g) {

        System.out.println("TEST1----------------------------");
        for (Stroke s: g.strokes())
            canvas.drawBox(s.box(), 0);
        // canvas.drawBox(g.box());
        canvas.drawStrokes(g.strokes());
    }

    private void test2(Graph gg) {
        System.out.println("TEST2----------------------------");

        Graph g = gg.clone();
        g.move(650, 300);

        /*
        // TODO: update this in brain as well
        // Get ratio
        int maxDimension = 0;
        for (Stroke s: g.strokes()) {
            if (s.box().width() > maxDimension)
                maxDimension = s.box().width();
            if (s.box().height() > maxDimension)
                maxDimension = s.box().height();
        }
        double ratio = 200.0/maxDimension;
        // Get center
        BoundingBox bbox = g.box();
        Point center = bbox.center;
        // resize
        for (Stroke s: g.strokes()) {
            s.resize(ratio, ratio, center);
        }
        */

        // canvas.drawBox(g.box());

        for (Stroke s: g.strokes()) {
            s.resample(2);
            s.smoothen(3);
            s.initLinearity();
        }

        for (Stroke s: g.strokes())
            canvas.drawBox(s.box(), 0);

        List<Stroke> strokes = g.strokes();

        Brain.splitHybridModifiers(strokes);

        try {
            // get internal segments
            Tuple<List<Graph>, Integer> returnVal = Segmentator.internalSegment(strokes);
            List<Graph> graphs = returnVal.first;
            int middleIndex = returnVal.second;

            // get yards
            List<Stroke> upperYard = new ArrayList<Stroke>();
            List<Stroke> lowerYard = new ArrayList<Stroke>();
            List<Stroke> middleYard = graphs.get(middleIndex).strokes();

            List<Stroke> copy = new ArrayList<Stroke>();
            for (Stroke s: middleYard)
                copy.add(s);

            if (middleIndex-1 >= 0)
                upperYard = graphs.get(middleIndex-1).strokes();
            if (middleIndex+1 < graphs.size())
                lowerYard = graphs.get(middleIndex+1).strokes();


            // Get tallest Stroke
            Stroke tallestStroke = Collections.max(middleYard, new Comparator<Stroke>() {
                @Override
                public int compare(Stroke me, Stroke other) {
                    return me.box().height() - other.box().height();
                }
            });

            // split ka and fa
            Brain.splitKaFa(middleYard, tallestStroke.box().height());

            // remove verticals from middle zone
            Tuple<Integer, Integer> removals = Brain.removeVerticals(middleYard, tallestStroke.box().height());
            int preVerticals = removals.first;
            int postVerticals= removals.second;

            copy.subList(preVerticals, copy.size()-postVerticals).clear();

            // For middleyard
            Stroke connected = Stroke.connect(middleYard);
            connected.resample(2);
            connected.smoothen(2);

            canvas.drawStrokes(copy, Color.green);
            canvas.drawStroke(connected, Color.darkGray);
            canvas.drawStrokes(upperYard, Color.red);
            canvas.drawStrokes(lowerYard, Color.blue);

        } catch (ListEmptyError ls) {
            System.out.println("Some error");
        }
    }

    public ViewWindow(String filename) {
        // set drawable to true
        super(false);


        AbstractAction mark = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {

                Graph g = bag.graphs().get(viewindex);
                String label = "x"+g.label();
                g.label(label);
                textfield.setText(g.label());
                bag.store();
                System.out.println("ViewWindow.mark(): Graph label has been marked.");

            }
        };

        AbstractAction unmark = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {

                Graph g = bag.graphs().get(viewindex);
                String label = g.label().substring(1);
                if (g.label().substring(0, 1).equals("x")) {
                    g.label(label);
                    textfield.setText(g.label());
                    bag.store();
                    System.out.println("ViewWindow.mark(): Graph label has been marked.");
                }
            }
        };

        AbstractAction previous = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {

                viewindex--;
                if (viewindex < 0)
                    viewindex = bag.graphs().size()-1;
                test();
            }
        };

        AbstractAction next = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {

                viewindex++;
                if (viewindex >= bag.graphs().size())
                    viewindex = 0;
                if (bag.graphs().size() <= 0)
                    return;
                test();

            }
        };

        AbstractAction renameOrRemove = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                if (bag.graphs().size() <= 0)
                    return;
                Graph g = bag.graphs().get(viewindex);
                String label = textfield.getText();


                if (label.equals("")) {
                    // remove
                    System.out.println("ViewWindow.train(): Nothing has been written.");
                    bag.remove(viewindex);
                    if (viewindex >= bag.graphs().size())
                        viewindex = bag.graphs().size()-1;

                    bag.store();
                    System.out.println("ViewWindow.remove(): Graph has been removed.");

                    if (bag.graphs().size() <= 0)
                        return;
                    test();
                } else {
                    // rename
                    g.label(label);
                    bag.store();
                    System.out.println("ViewWindow.train(): Graph label has been renamed.");
                }
            }
        };

        // Creates mark button
        JButton markButton = new JButton("Mark");
        markButton.addActionListener(mark);
        panel.add(markButton);

        // Creates unmark button
        JButton unmarkButton = new JButton("Unmark");
        unmarkButton.addActionListener(unmark);
        panel.add(unmarkButton);

        // Creates prev button
        JButton prevButton = new JButton("Previous");
        prevButton.addActionListener(previous);
        panel.add(prevButton);

        // Creates next button
        JButton nextButton = new JButton("Next");
        nextButton.addActionListener(next);
        panel.add(nextButton);


        textfield.getInputMap().put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        textfield.getActionMap().put("Enter", renameOrRemove);

        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "Left");
        panel.getActionMap().put("Left", previous);
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "Right");
        panel.getActionMap().put("Right", next);

        bag = bag.load(filename);

        for (Graph g: bag.graphs())
            g.move(250, 300);
        viewindex = bag.graphs().size()-1;

    }
}
