package io.gitlab.nprs.nhr.gui;

import java.util.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import io.gitlab.nprs.nhr.engine.Stroke;
import io.gitlab.nprs.nhr.engine.Graph;
import io.gitlab.nprs.nhr.engine.Bag;
import io.gitlab.nprs.nhr.engine.ListEmptyError;

public class TrainWindow extends Window {

    private Bag bag;

    // train the active strokes in canvas
    private void train(String label) {
        List<Stroke> strokes = canvas.strokes();
        if (label.equals("")) {
            System.out.println("TrainWindow.train(): Nothing has been written.");
        } else if (strokes.isEmpty()) {
            System.out.println("TrainWindow.train(): Nothing has been drawn.");
        } else {
            try {
                Graph graph = new Graph(strokes, label);
                bag.append(graph);
                bag.store();
                canvas.garbage = true;
            } catch (ListEmptyError ls) {
                System.out.println(ls.msg);
            }

        }
    }

    public TrainWindow(String filename) {
        // set drawable to true
        super(true);

        // popluate training characters for middle zone characters
        String trainingSet = "क ख ग घ ङ " +
            "च छ ज झ ञ " +
            "ट ठ ड ढ ण " +
            "त थ द ध न " +
            "प फ ब भ म " +
            "य र ल व " +
            "श ष स ह " +
            "क्ष त्र ज्ञ " +
            "अ इ उ ऊ ऋ ए " +
            "० १ २ ३ ४ ५ ६ ७ ८ ९ "+
            " े ै ु ू";
        textfield.setText(trainingSet);

        // create skip button
        JButton skipButton = new JButton("Skip");
        skipButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String text = textfield.getText();
                String[] part = text.split(" ", 2);
                String character = part[0];
                String updateText = (part.length==1)?"":part[1];

                textfield.setText(updateText);

                // VERBOSE:
                System.out.println("Skipped a character "+character);
            }
        });
        panel.add(skipButton);

        // create trainandnext button
        JButton trainandnextButton = new JButton("Train and Next");
        trainandnextButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                // Get the text from the textfield
                String text = textfield.getText();
                String[] part = text.split(" ", 2);

                // Pop the first character
                String updateText = (part.length==1)?"":part[1];
                textfield.setText(updateText);

                // Train the first character
                String character = part[0];
                train(character);

                // VERBOSE:
                System.out.println("Trained a character "+character);
            }
        });
        panel.add(trainandnextButton);

        // create train button
        JButton trainButton = new JButton("Train");
        trainButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                // Get the text from the textfield
                String text = textfield.getText();
                String[] part = text.split(" ", 2);

                // Train the first character
                String character = part[0];
                train(character);

                // VERBOSE:
                System.out.println("Trained a character "+character);
            }
        });
        panel.add(trainButton);

        // for saving training data
        bag = Bag.load(filename);
    }
}
