package io.gitlab.nprs.nhr.engine;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import io.gitlab.nprs.nhr.engine.Options.Model;

public class ModelsFile {

    public String filename;
    public Model model;
    public LearningModel<LabeledStroke> upper;
    public LearningModel<LabeledStroke> middle;
    public LearningModel<LabeledStroke> lower;

    public ModelsFile(String filename, Model model) {
        this.filename = filename;
        this.model = model;
    }

    public void instantiateAll() {
        upper = instantiateModel(model);
        middle = instantiateModel(model);
        lower = instantiateModel(model);
    }

    public void readModels() throws IOException {
        File file = new File(filename);
        // If file already there, try loading models
        if (file.exists() && !file.isDirectory()) {
            FileInputStream fis = new FileInputStream(file);
            ZipInputStream zis = new ZipInputStream(fis);
            instantiateAll();
            ZipEntry zentry;
            while ((zentry=zis.getNextEntry())!=null) {
                LearningModel<LabeledStroke> mdl = getModelRef(zentry.getName());
                System.out.println("Reading-"+zentry.getName()+"-");
                mdl.load(zis);
                zis.closeEntry();
            }
            zis.close();
            fis.close();
        } else {
            throw new FileNotFoundException("Model file not found.");
        }
    }

    public void writeModels() throws IOException {
        if (upper==null || lower==null || middle==null)
            throw new IOException("Models couldn't be written. One of them null.");
        FileOutputStream fos = new FileOutputStream(new File(filename));
        ZipOutputStream zos = new ZipOutputStream(fos);
        LearningModel<LabeledStroke> mdl;
        String[] modelNames = {"upper","middle","lower"};
        for (String mdlName : modelNames) {
            System.out.println("Writing "+mdlName);
            zos.putNextEntry(new ZipEntry(mdlName));
            mdl = getModelRef(mdlName);
            mdl.save(zos);
            zos.closeEntry();
        }
        zos.close();
        fos.close();
    }

    private LearningModel<LabeledStroke> getModelRef(String fname) {
        if (Objects.equals(fname.toLowerCase(),"upper")) {
            return upper;
        } else if (Objects.equals(fname.toLowerCase(),"lower")) {
            return lower;
        } else
            return middle;
    }

    private static LearningModel<LabeledStroke> instantiateModel(Model model) {
        LearningModel mdl;
        if (model==Model.KNN)
            // whatever. this doesn't matter now, but TODO
            mdl = new KNN<LabeledStroke>(7,new DTW<LabeledStroke>());
        else if (model==Model.ANN)
            mdl = new ANNEnc<LabeledStroke>();
        else
            mdl = new SVMEnc<LabeledStroke>();
        return mdl;
    }
}
