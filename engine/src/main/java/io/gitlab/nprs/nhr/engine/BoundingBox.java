package io.gitlab.nprs.nhr.engine;

import java.util.List;

public class BoundingBox implements Comparable<BoundingBox> {

    public Point center;
    public Point topLeft;
    public Point bottomRight;

    public Point summation;
    public int count;

    public BoundingBox(List<Point> points) throws ListEmptyError {
        if (points.isEmpty())
            throw new ListEmptyError("BoundingBox.BoundingBox");

        topLeft = points.get(0).clone();
        bottomRight = points.get(0).clone();
        center = new Point(0, 0);
        summation= new Point(0, 0);
        for (Point point: points) {
            topLeft.x = Math.min(topLeft.x, point.x);
            topLeft.y = Math.min(topLeft.y, point.y);
            bottomRight.x = Math.max(bottomRight.x, point.x);
            bottomRight.y = Math.max(bottomRight.y, point.y);
            summation.x += point.x;
            summation.y += point.y;
        }
        count = points.size();
        center.x = summation.x/count;
        center.y = summation.y/count;
    }

    // Create bounding box from topLeft and bottomRight points
    public BoundingBox(Point topLeft, Point bottomRight) {
        // FIXME: Potential error here
        if (topLeft.y > bottomRight.y || topLeft.x > bottomRight.x) {
            System.out.println("Error in topLeft and bottomRight assignment");
        }

        this.topLeft = topLeft.clone();
        this.bottomRight = bottomRight.clone();
        count = 2;
        summation = new Point(topLeft.x + bottomRight.x, topLeft.y+bottomRight.y);
        center = new Point(summation.x/count, summation.y/count);
    }

    private BoundingBox() {
        // private bounding box constructor
    }

    public BoundingBox clone() {
        BoundingBox tmp = new BoundingBox();
        tmp.topLeft = topLeft.clone();
        tmp.bottomRight = bottomRight.clone();
        tmp.center = center.clone();
        tmp.summation = summation.clone();
        tmp.count = count;
        return tmp;
    }

    public void merge(BoundingBox box) {
        topLeft.x = Math.min(topLeft.x, box.topLeft.x);
        topLeft.y = Math.min(topLeft.y, box.topLeft.y);
        bottomRight.x = Math.max(bottomRight.x, box.bottomRight.x);
        bottomRight.y = Math.max(bottomRight.y, box.bottomRight.y);
        summation.x += box.summation.x;
        summation.y += box.summation.y;
        count += box.count;
        center.x = summation.x/count;
        center.y = summation.y/count;
    }

    public int height() {
        return bottomRight.y - topLeft.y;
    }

    public int width() {
        return bottomRight.x - topLeft.x;
    }

    public int area() {
        return (bottomRight.x-topLeft.x)*(bottomRight.y-topLeft.y);
    }

    public int overlapArea(BoundingBox t) {
        int width = (Math.min(t.bottomRight.x,bottomRight.x) - Math.max(t.topLeft.x, topLeft.x));
        int height = (Math.min(t.bottomRight.y, bottomRight.y) - Math.max(t.topLeft.y, topLeft.y));
        // if there is no overlap
        if (height<=0 || width<=0)
            return 0;
        return height*width;
    }

    public void move(int offsetx, int offsety) {
        center.shift(offsetx, offsety);
        topLeft.shift(offsetx, offsety);
        bottomRight.shift(offsetx, offsety);
        summation.shift(offsetx*count, offsety*count);
    }

    public void resize(double ratioX, double ratioY, Point center) {
        this.center.resize(ratioX, ratioY, center);
        topLeft.resize(ratioX, ratioY, center);
        bottomRight.resize(ratioX, ratioY, center);
        summation.resize(ratioX, ratioY, new Point(center.x*count, center.y*count));
    }

    // return the horizontal space between two bounding boxes
    // negative value for overlapped space
    public int hSpace(BoundingBox t) {
        return Math.max(topLeft.x, t.topLeft.x) - Math.min(bottomRight.x, t.bottomRight.x);
    }

    // return the vertical space between two bounding boxes
    // negative value for overlapped space
    public int vSpace(BoundingBox t) {
        return Math.max(topLeft.y, t.topLeft.y) - Math.min(bottomRight.y, t.bottomRight.y);
    }

    public double vOverlapRatio(BoundingBox box) {
        return -1.0*vSpace(box)/Math.min(height()+1, box.height()+1);
    }

    public double hOverlapRatio(BoundingBox box) {
        return -1.0*hSpace(box)/Math.min(width()+1, box.width()+1);
    }

    // grow dots
    private void grow() {
        if (width() <= 1) {
            topLeft.x -= 1;
            bottomRight.x += 1;
        }
        if (height() <= 1) {
            topLeft.y -= 1;
            bottomRight.y += 1;
        }
    }

    public int compareTo(BoundingBox box) {
        int sign = 1;
        BoundingBox s = box.clone();
        BoundingBox t = this.clone();

        // take the widest as the base for comparision
        if (t.height() < s.height()) {
            sign = -1;
            // swapping
            BoundingBox x = s;
            s = t;
            t = x;
        }

        // grow points and lines
        s.grow();
        t.grow();

        // Get total area of the block in question
        int totalArea = s.area();
        int totalOverlapArea = 0;


        // NOTE:
        // Going from last to first in overlap areas

        // One-third and Two-third position of BoundingBox t in x axis
        int onethirdx = t.topLeft.x + 1*(t.bottomRight.x - t.topLeft.x)/3;
        int twothirdx = t.topLeft.x + 2*(t.bottomRight.x - t.topLeft.x)/3;

        int onethirdy = t.topLeft.y + 1*(t.bottomRight.y - t.topLeft.y)/3;
        int twothirdy = t.topLeft.y + 2*(t.bottomRight.y - t.topLeft.y)/3;

        // Get intersection area after the block
        {
            Point topLeft = new Point(t.bottomRight.x, Integer.MIN_VALUE);
            Point bottomRight = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
            totalOverlapArea += s.overlapArea(new BoundingBox(topLeft, bottomRight));
        }

        // last part
        {
            Point topLeft = new Point(twothirdx, t.topLeft.y);
            Point bottomRight = new Point(t.bottomRight.x, Integer.MAX_VALUE);
            totalOverlapArea += s.overlapArea(new BoundingBox(topLeft, bottomRight));
        }

        // middle part
        {
            Point topLeft = new Point(onethirdx, onethirdy);
            Point bottomRight = new Point(twothirdx, Integer.MAX_VALUE);
            totalOverlapArea += s.overlapArea(new BoundingBox(topLeft, bottomRight));
        }

        // first part
        {
            Point topLeft = new Point(t.topLeft.x, twothirdy);
            Point bottomRight = new Point(onethirdx, Integer.MAX_VALUE);
            totalOverlapArea += s.overlapArea(new BoundingBox(topLeft, bottomRight));
        }

        // if return value is greater than zero, then block is smaller
        return sign * (totalArea/2 - totalOverlapArea);
    }

}
