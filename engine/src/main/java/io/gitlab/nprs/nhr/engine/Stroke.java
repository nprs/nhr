package io.gitlab.nprs.nhr.engine;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import java.lang.Math;

public class Stroke implements Comparable<Stroke> {

    protected BoundingBox bbox;
    protected List<Point> lst;

    private double sineSum;
    private double cosineSum;

    public boolean linear;
    public double orientation;

    {
        linear = false;
        orientation = 0.0;
    }

    // private constructor
    private Stroke() {
    }

    // copy constructor
    protected Stroke(Stroke stroke) {
        lst = new ArrayList<Point>();
        for (Point p: stroke.lst)
            lst.add(p.clone());

        bbox = stroke.bbox.clone();

        linear = stroke.linear;
        orientation = stroke.orientation;

        sineSum = stroke.sineSum;
        cosineSum = stroke.cosineSum;
    }

    // constructor
    public Stroke(List<Point> points) throws ListEmptyError {
        lst = new ArrayList<Point>();
        // no need to copy empty stroke
        if (points.isEmpty())
            throw new ListEmptyError("Stroke.Stroke");
        // copy all the elements
        for (Point p: points)
            lst.add(p);

        bbox = new BoundingBox(lst);

        initLinearity();
    }

    // create a stroke from list of strokes, joining the last point to previous
    // stroke with first point of next stroke
    public static Stroke connect(List<Stroke> strokes) throws ListEmptyError {
        if (strokes.isEmpty())
            throw new ListEmptyError("Stroke.Stroke");

        Stroke tmp = new Stroke();

        tmp.lst = new ArrayList<Point>();
        for (Stroke s: strokes)
            for (Point p: s.lst)
                tmp.lst.add(p.clone());

        tmp.bbox = strokes.get(0).bbox.clone();

        if (strokes.size() == 1) {
            tmp.linear = strokes.get(0).linear;
            tmp.orientation = strokes.get(0).orientation;

            tmp.sineSum = strokes.get(0).sineSum;
            tmp.cosineSum = strokes.get(0).cosineSum;
        } else {

            for (int i=1; i<strokes.size(); i++)
                tmp.bbox.merge(strokes.get(i).bbox);

            tmp.initLinearity();
        }

        return tmp;
    }

    // make stroke suitable for further processing
    public Stroke sanitize() {
        // normalize the max dimension to 120 preserving the ratio
        normalize(120, true);
        // fine resampling for smoothing
        resample(2);
        // smoothing
        smoothen(2);
        // coarse resampling
        resample(3);

        // move to some place
        move(60, 60);

        return this;
    }

    public Stroke clone() {
        return new Stroke(this);
    }

    // identify linearity parameters for a stroke
    public void initLinearity() {
        // defines the minimum dispersion value for stroke to be linear
        double threshold = 0.94;
        // defines the separation between points in pair for orientation calculation
        int separation = 5;
        if (separation >= lst.size()) {
            linear = false;
            orientation = 0;
            return;
        }

        // stores sum of sine and cosine of vectors
        double sine = 0;
        double cosine = 0;
        for (int i=0; i+separation<lst.size();i++){
            // get the pairs for orientation calculation
            Point A = lst.get(i);
            Point B = lst.get(i+separation);

            double p = B.y - A.y;
            double b = B.x - A.x;
            double h = Math.sqrt(p*p + b*b);
            sine += p/h;
            cosine += b/h;
        }

        this.sineSum = sine;
        this.cosineSum = cosine;

        sine /= (lst.size()-separation);
        cosine /= (lst.size()-separation);

        // value of dispersion, 1 means concentration in one direction
        double dispersion = Math.sqrt(sine*sine + cosine*cosine);

        // average orientation of the stroke
        orientation = Math.toDegrees(Math.acos(cosine));

        // fix orientation
        if (sine < 0)
            orientation = 360 - orientation;

        // a stroke is linear if standard deviation is below certain threshold
        linear = (dispersion >= threshold);

    }


    // just modulo operation, that also works for negative
    private static double modulo(double a, double m) {
        double result = a;
        while (result<0)
            result+=m;
        return result % m;
    }

    // distance between two numbers in clock-wise direction
    private static double moduloDistance(double x, double y, double m) {
        double a = modulo(x, m);
        double b = modulo(y, m);

        double dist = b - a;
        while (dist<0.0)
            dist += 360.0;
        return dist;
    }

    private static boolean isNear(double target, double basis, double tolerance) {
        double md1 = moduloDistance(basis-tolerance, target, 360);
        double md2 = 2*tolerance;
        return (md1 <= md2);
    }


    // if a stroke is linear, and has a certain angle, given the tolerance
    public boolean isNear(double basis, double tolerance) {
        // NOTE: tolerance of 20 is good
        // determines if given stroke is vertical or horizontal with reference to 45 and 135 degree
        return linear && isNear(orientation, basis, tolerance);
    }

    // if a stroke is linear
    public boolean isLinear() {
        return linear;
    }

    // Convert to a string representation
    public String toString() {
        String strokestr = "";
        for (Point pt : lst) {
            strokestr += pt.toString() + " ";
        }
        return strokestr;
    }

    // FIXME: Potential error here
    public static Stroke fromString(String rep) throws ListEmptyError {
        List<Point> ptsarray = new ArrayList<Point>();

        String[] pts = rep.split(" ");
        for (String pt : pts)
            ptsarray.add(Point.fromString(pt));

        return new Stroke(ptsarray);
    }

    public Point firstPoint() {
        return lst.get(0);
    }

    public Point midPoint() {
        return lst.get(lst.size()/2);
    }

    public Point lastPoint() {
        return lst.get(lst.size()-1);
    }

    public List<Point> points() {
        return lst;
    }

    public BoundingBox box() {
        return bbox;
    }

    @Override
    public int compareTo(Stroke stk) {
        return bbox.compareTo(stk.bbox);
    }

    public void resize(double ratioX, double ratioY, Point center) {
        for (Point p: lst)
            p.resize(ratioX, ratioY, center);
        // Update boundingbox
        bbox.resize(ratioX, ratioY, center);
    }

    // normalize a given stroke to certain size
    // NOTE: bounding box info isn't updated
    public void normalize(int size, boolean preserveRatio) {
        double hratio = (double)size/(bbox.height()+1);
        double vratio = (double)size/(bbox.width()+1);
        if (preserveRatio) {
            double maxratio = Math.min(hratio, vratio);
            hratio = maxratio;
            vratio = maxratio;
        }
        resize(vratio, hratio, bbox.center);
    }

    // Apply gaussian smoothing, appriximation must be greater than 4
    public void gaussianSmoothen(int approximation, int window) {
        for (int i=0; i<approximation; i++)
            smoothen(window);
    }

    // NOTE: linearity information isn't updated
    // Apply simple moving average (central)
    public void smoothen(int window) {
        int len = lst.size();
        // window size for central sma
        int doubleWindow = 2*window+1;
        // length of stroke must be greater than window size for smoothing
        if (doubleWindow >= len)
            return;

        // sma represent the simple moving average
        Point sma = new Point(0, 0);
        // list holding the smoothed result
        List<Point> smoothed = new ArrayList<Point>();

        // calculate sma for first window
        for (int k=-window;k<=window;k++){
            int index = k<0 ? 0 : k;
            sma.x += lst.get(index).x;
            sma.y += lst.get(index).y;
        }
        smoothed.add(new Point((int)Math.round((double)sma.x/doubleWindow),
                    (int)Math.round((double)sma.y/doubleWindow)));

        // calculate sma for remaining windows
        for (int k=1;k<len;k++){
            // remove first point of older window
            int index = k-1-window<0 ? 0 : k-1-window;
            sma.x -= lst.get(index).x;
            sma.y -= lst.get(index).y;
            // add point for new window
            index = k+window>len-1 ? len-1 : k+window;
            sma.x += lst.get(index).x;
            sma.y += lst.get(index).y;
            smoothed.add(new Point((int)Math.round((double)sma.x/doubleWindow),
                        (int)Math.round((double)sma.y/doubleWindow)));
        }

        // save smoothed stroke
        lst = smoothed;
    }

    // resample the stroke at a certain spacing
    public void resample(int spacing) {
        List<Point> resampled = new ArrayList<Point>();
        // Add first point
        resampled.add(lst.get(0));
        for (Point p: lst) {
            // Get latest point in the new ArrayList
            Point recent = resampled.get(resampled.size()-1);
            // only add point which are greater than certain distance
            double distance = recent.distance(p);
            if (distance < spacing)
                continue;
            // use segmentation formula to fill in points
            double numberOfSegment = distance/spacing;
            for (int i=1; i<numberOfSegment;i++){
                Point q = recent.divide(p, i/numberOfSegment);
                resampled.add(q);
            }
        }
        // save resampled stroke
        lst = resampled;
    }

    public Stroke reverse() {
        Collections.reverse(lst);
        // update linearity information
        orientation = (orientation+180) % 360;
        sineSum = -sineSum;
        cosineSum = -cosineSum;
        return this;
    }

    public static Stroke tallest(List<Stroke> strokes) {
        Stroke tallest = strokes.get(0);
        for (Stroke testStroke: strokes) {
            if (testStroke.box().height() >= tallest.box().height())
                tallest = testStroke;
        }
        return tallest;
    }

    // move such that the new center is at (x, y)
    public void move(int x, int y) {
        int offsetx = x-box().center.x;
        int offsety = y-box().center.y;
        shift(offsetx, offsety);
    }

    // shift through some offset
    public void shift(int offsetx, int offsety) {
        for (Point p: lst)
            p.shift(offsetx, offsety);
        bbox.move(offsetx, offsety);
    }

    // FIXME: what if splitIndex is greater than list size?
    public List<Stroke> splitAt(int splitIndex) {
        List<Stroke> splittedStrokes = new ArrayList<Stroke>();
        if (splitIndex <= 0)
            splitIndex = lst.size()-1;
        try {
            List<Point> partA = lst.subList(0, Math.min(splitIndex, lst.size()-1)+1);
            Stroke strokeA = new Stroke(partA);
            splittedStrokes.add(strokeA);

            if (splitIndex < lst.size()-1) {
                List<Point> partB = lst.subList(splitIndex, lst.size());
                Stroke strokeB = new Stroke(partB);
                splittedStrokes.add(strokeB);
            }
        } catch (ListEmptyError e) {
            // Just ignore, should never occur
            System.out.println(e.msg);
            System.out.println("ERROR: Couldn't create the second part");
        }

        return splittedStrokes;
    }

    // splits a stroke into two parts, given x co-ordinate of vertical line
    public List<Stroke> splitVertical(int vpos) {
        int splitIndex = lst.size();
        for(int i=lst.size()-1; i>=0; i--) {
            if (lst.get(i).x < vpos) {
                splitIndex = i;
                break;
            }
        }
        return splitAt(splitIndex);
    }

    public List<Stroke> splitLinear() {
        // angle of linear
        double angle = 270;
        // tolerance for angle of linear 
        double tolerance = 15;

        // defines the minimum dispersion value for stroke to be linear
        double threshold = 0.99;
        // defines the separation between points in pair for orientation calculation
        int separation = 5;

        // don't split strokes that have very small number of points
        if (separation >= lst.size()) {
            System.out.println("FAIL: Very small stroke for linear split.");
            return null;
        }

        // i is the index to which it is linear
        int i=0;
        // stores sum of sine and cosine of vectors
        double sineSum  = this.sineSum;
        double cosineSum = this.cosineSum;
        for (i=lst.size()-1-separation; i>0;i--){
            // get the pairs for orientation calculation
            Point A = lst.get(i);
            Point B = lst.get(i+separation);
            // get p, b, h
            double p = B.y - A.y;
            double b = B.x - A.x;
            double h = Math.sqrt(p*p + b*b);

            sineSum -= p/h;
            cosineSum -= b/h;

            double sine = sineSum/(i);
            double cosine = cosineSum/(i);

            // value of dispersion, 1 means concentration in one direction
            double dispersion = Math.sqrt(sine*sine + cosine*cosine);
            double orientation = Math.toDegrees(Math.acos(cosine));
            if (sine < 0)
                orientation = 360 - orientation;

            // dispersion must be near 1, orientation must be near 270
            boolean linear = (dispersion >= threshold);
            boolean op = isNear(orientation, angle, tolerance);

            // break if the condition isn't satisfied
            if (linear && op)
                break;
        }

        // Get value of first split point
        int first = (int)(i*0.95);
        if (first<=0 || first==lst.size()-1) {
            System.out.println("FAIL: No linear part found for linear split.");
            return null;
        }

        List<Stroke> values = splitAt(first);
        // reverse the 270 degree facing linear
        values.get(0).reverse();
        return values;
    }

    public List<Stroke> splitNonLinear(Stroke linear) {
        // Get value of second spllit point
        int second = lst.size()-1;

        int j=0;
        for (j=0; j<lst.size(); j++) {
            if (lst.get(j).y < linear.firstPoint().y)
                break;
        }

        for (; j<lst.size(); j++) {
            if (lst.get(j).y > linear.firstPoint().y) {
                second = j;
                break;
            }
        }

        if (second<=0 || second >= lst.size()-1) {
            System.out.println("ERROR: No modifier part found for nonlinear split.");
            return null;
        }

        return splitAt(second);
    }

    public List<Stroke> splitSmart(Stroke givenLinear) {
        Stroke modifier = null;
        Stroke nonlinear = null;
        Stroke linear = null;

        Stroke temporary = null;

        // System.out.println("STATUS: System size is "+lst.size());

        // if linear isn't given then try splitting itself
        if (givenLinear == null) {
            List<Stroke> linearSplit = splitLinear();
            // Couldn't even split the linear
            if (linearSplit == null) {
                System.out.println("FAIL: Couldn't get linear for smart split");
                return null;
            }
            linear = linearSplit.get(0);
            temporary = linearSplit.get(1);
        } else {
            linear = givenLinear;
            temporary = this;
        }
        // System.out.println("STATUS: Linear size is "+linear.points().size());
        // System.out.println("STATUS: Temporary size is "+temporary.points().size());

        List<Stroke> nonlinearSplit = temporary.splitNonLinear(linear);
        if (nonlinearSplit == null) {
            modifier = temporary;
            // System.out.println("STATUS: Modifier size is "+modifier.points().size());
        } else {
            modifier = nonlinearSplit.get(0);
            nonlinear = nonlinearSplit.get(1);

            // System.out.println("STATUS: Modifier size is "+modifier.points().size());
            // System.out.println("STATUS: Nonlinear size is "+nonlinear.points().size());
        }

            // NEW FEATURE: Only split ikar
        //    if (givenLinear != null) {
            if (modifier.midPoint().y > modifier.firstPoint().y ||
                    modifier.midPoint().y > modifier.lastPoint().y) {
                System.out.println("ERROR: Not a ukar");
                return null;
            }
        //    }



        double linearH = linear.box().height();
        // FIXME:
        // double modifierH = Math.max(modifier.box().height(), modifier.box().width());
        double modifierH = modifier.box().width();


        // Check for size 3
        if (nonlinear != null) {
            double nonlinearH = nonlinear.box().height();

            // Modifier's top shouldn't be below non-linear's top
            if (modifier.box().topLeft.y > nonlinear.box().topLeft.y) {
                System.out.println("FAIL: Modifier's top-boundary  is below non-linear's top-boundary");
                return null;
            }

            // Modifier's bottom shouldn't be below non-linear's center
            if (modifier.box().bottomRight.y > nonlinear.box().center.y) {
                System.out.println("FAIL: Modifier's bottom  is below non-linear's center");
                return null;
            }

            if (modifier.firstPoint().x > modifier.lastPoint().x && modifier.firstPoint().x < nonlinear.box().center.x ||
                    modifier.firstPoint().x < modifier.lastPoint().x && modifier.firstPoint().x > nonlinear.box().center.x) {
                System.out.println("FAIL: Non-linear's center is inconsistent with respect to modifier");
                return null;
            }

            System.out.println(nonlinear.lastPoint().y+" < "+linear.box().center.y);
            boolean nonlinearIsLinear = nonlinear.isNear(90, 20) && nonlinear.lastPoint().y < linear.box().center.y;
            if (nonlinearIsLinear)
                System.out.println("WARNING: Nonlinear is linear, but very small");

            System.out.println("nl/l :"+1.0*nonlinearH/linearH+" must be above 0.4");
            boolean nonlinearIsSmall = 1.0*nonlinearH/linearH< 0.4;
            if (nonlinearIsSmall)
                System.out.println("WARNING: Nonlinear is very smaller than linear");

            if (nonlinearIsLinear || nonlinearIsSmall) {

                // NOTE: if linear is given then, there should be a nonlinear
                if (givenLinear != null) {
                    System.out.println("FAIL: No non-linear was found");
                    return null;
                }

                // get new point to split such that no second split can be made
                int splitPoint = linear.lst.size()-1;
                for (int i=linear.lst.size()-1; i>=0; i--) {
                    if (lst.get(i).y >= lst.get(lst.size()-1).y) {
                        splitPoint = i;
                        break;
                    }
                }

                // Non-linear isn't needed
                nonlinear = null;

                if (splitPoint<=0 || splitPoint >=linear.lst.size()-1) {
                    // This failed
                    System.out.println("FAIL: Couldn't smart split");
                } else {
                    System.out.println("SUCCESS: Smart splitted.");
                    List<Stroke> smartSplit = splitAt(splitPoint);
                    linear = smartSplit.get(0);
                    linear.reverse();
                    modifier = smartSplit.get(1);
                }
            }
        }

        // Modifier's top shouldn't be below linear's top
        if (modifier.box().topLeft.y > linear.box().topLeft.y) {
            System.out.println("FAIL: Modifier's top-boundary  is below linear's top-boundary");
            return null;
        }

        // Modifier's bottom shouldn't be below linear's center
        if (modifier.box().bottomRight.y > linear.box().center.y) {
            System.out.println("FAIL: Modifier's bottom  is below linear's center");
            return null;
        }

        System.out.println("l/m: "+1.0*linearH/modifierH+" must be above 0.4");
        if (1.0*linearH/modifierH < 0.4) {
            System.out.println("FAIL: Linear is very smaller than modifier");
            return null;
        }

        System.out.println("m/l :"+1.0*modifierH/linearH+" must be above 0.1");
        if (1.0*modifierH/linearH < 0.1) {
            System.out.println("WARNING: Modifier is very smaller than linear");
            // Modifier can be very small if nonlinear is linear
            if (nonlinear!=null && nonlinear.isLinear())
                System.out.println("STATUS: Nonlinear is linear, no need to remove modifier");
            else {
                System.out.println("FAIL: Modifier was tooo small");
                return null;
            }
        }


        List<Stroke> returnVals = new ArrayList<Stroke>();
        returnVals.add(linear);
        returnVals.add(modifier);
        if (nonlinear != null)
            returnVals.add(nonlinear);
        System.out.println("STATUS: Splitted at "+returnVals.size()+" parts");
        return returnVals;
    }


    // calculate the first difference of the stroke
    public List<Vector> firstDifference() {
        List<Vector> differences = new ArrayList<Vector>();
        Point old = lst.get(0);
        for (int i=1; i<lst.size(); i++) {
            Point current = lst.get(i);
            Vector difference = new Vector(old, current);
            differences.add(difference);
            old = current;
        }
        return differences;
    }

    /* Calculate the Discrete Cosine Transform of the
     * Stroke. This is a hand-coded O(n^2) implementation
     * of the DCT-II formula.
     */
    public List<double[]> dct() {
        int N = lst.size();
        double[] X = new double[N];
        double[] Y = new double[N];
        List<double[]> dcts = new ArrayList<double[]>();
        double costerm;
        for (int k=0; k<N; k++) {
            X[k] = 0; Y[k] = 0;
            for (int n=0; n<N; n++) {
                costerm =  Math.cos(Math.PI*k*(n+0.5)/N);
                X[k] += lst.get(n).x*costerm;
                Y[k] += lst.get(n).y*costerm;
            }
        }
        dcts.add(X); dcts.add(Y);
        return dcts;
    }

    /* Return a feature vector for the stroke, which is a vector
     * of halfVectSize*2 real numbers, the first halfVectSize and
     * the second halfVectSize being truncated DCTs of the stroke.
     */
    public double[] featureVect() {
        List<double[]> dcts = this.dct();
        int halfVectSize = 14;
        double[] fvect = new double[halfVectSize*2];
        for (int i=0; i<halfVectSize*2; i++) {
            if ((dcts.get(i/halfVectSize).length-1)<(i%halfVectSize))
                fvect[i] = 0;
            else
                fvect[i] = dcts.get(i/halfVectSize)[i%halfVectSize];
        }
        // fvect[0] = 0;
        // fvect[halfVectSize] = 0;
        return fvect;
    }

    private double log2(double x) {
        return Math.log(x)/Math.log(2);
    }
}

