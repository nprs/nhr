package io.gitlab.nprs.nhr.engine;

import java.util.List;
import java.util.ArrayList;

public class DTW<T extends Stroke> implements Metric<T> {

    public double distance(T x, T y) {

        List<Point> a = x.points();
        List<Point> b = y.points();

        /*
            // normalization in position is required
            // this is done later so that the output is better visualized
            Stroke A = x.clone();
            Stroke B = y.clone();
            B.move(400, 300);

            List<Point> a = A.points();
            List<Point> b = B.points();
        */

        /*
           // no normalization is required
            List<Vector> a = x.firstDifference();
            List<Vector> b = y.firstDifference();
        */

        int len_a = a.size();
        int len_b = b.size();
        double[][] D = new double[len_a+1][len_b+1];
        for (int i=0; i<len_a; i++) {
            for (int j=0; j<len_b; j++)
                D[i][j] = 10E16;
        }
        D[0][0] = 0;
        for (int i=0; i<len_a; i++) {
            for (int j=0; j<len_b; j++) {
                double cost = a.get(i).dissimilarity(b.get(j));
                D[i+1][j+1] = cost+Math.min(D[i][j+1],Math.min(D[i+1][j],D[i][j]));
            }
        }
        return D[len_a][len_b];
    }

}
