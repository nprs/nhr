package io.gitlab.nprs.nhr.engine;

public class Point {

    public int x;
    public int y;

    // constructor
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    private Point(Point p) {
        x = p.x;
        y = p.y;
    }

    public Point clone() {
        return new Point(this);
    }

    // FIXME: Potential error here
    public static Point fromString(String rep) {
        String[] splitted = rep.split(",");
        return new Point(Integer.parseInt(splitted[0]),
                         Integer.parseInt(splitted[1]));
    }

    public void shift(int offsetx, int offsety) {
        x += offsetx;
        y += offsety;
    }

    // Convert to a string representation
    public String toString() {
        return String.valueOf(x)+","+String.valueOf(y);
    }

    // get distance with point p
    public double distance(Point p){
        return Math.sqrt(Math.pow(x-p.x, 2)+Math.pow(y-p.y, 2));
    }

    // get point such that its distance between points is in ratio r:(1-r)
    public Point divide(Point p, double r){
        int sx = Segmentator.segment(x, p.x, r);
        int sy = Segmentator.segment(y, p.y, r);
        return new Point(sx, sy);
    }

    public double dissimilarity(Point p) {
        return distance(p);
    }


    // TEMP
    public void resize(double ratioX, double ratioY, Point center) {
        x = (int)((x-center.x)*ratioX) + center.x;
        y = (int)((y-center.y)*ratioY) + center.y;
    }


}
