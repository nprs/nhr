package io.gitlab.nprs.nhr.engine;

public interface Metric<T> {

    public double distance(T a, T b);

}
