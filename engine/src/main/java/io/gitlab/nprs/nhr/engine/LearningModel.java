package io.gitlab.nprs.nhr.engine;

import java.util.List;
import java.util.Objects;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* interface LearningModel
 * Represents a machine learning model.
 */
abstract public class LearningModel<E extends Example> {

    protected List<E> testSet;

    /* Load a trained model from a file
     */
    abstract public void load(InputStream is) throws IOException;

    /* Add a training example to the model, before
     * training.
     */
    abstract public void addExample(E data);

    /* Used all the added training examples to train the
     * model.
     */
    abstract public void train();

    /* Save the trained model to a given file.
     */
    abstract public void save(OutputStream os) throws IOException;

    /* Classify the input using the model.
     */
    abstract public String classify(E input);

    abstract public List<String> classifyAndSuggest(E input);

    public int misclassifiedTest() {
        int testSetSize = testSet.size();
        int mistakes = 0;
        String lbl;
        for (E ex : testSet) {
            lbl = classify(ex);
            if (!Objects.equals(lbl,ex.label()))
                mistakes++;
        }
        return mistakes;
    }

    public void reportAccuracy() {
        int testSetSize = totalTest();
        int mistakes = misclassifiedTest();
        System.out.println("LearningModel.reportAccuracy(): Accuracy = "+
                (double)(testSetSize-mistakes)*100/(double)testSetSize + "%");
    }

    public int totalTest() {
        return testSet.size();
    }

    public TestReport report() {
        TestReport rep = new TestReport();
        rep.total = totalTest();
        rep.misclassified = misclassifiedTest();
        return rep;
    }

}
