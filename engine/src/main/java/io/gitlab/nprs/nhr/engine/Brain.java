package io.gitlab.nprs.nhr.engine;

import java.util.Comparator;
import java.util.ListIterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

import io.gitlab.nprs.nhr.engine.Options.Model;

// TODO:
// don't break using linear if linear is empty

public class Brain {

    // Train classifiers with data from bags

    public static void learn(Bag bag,
            LearningModel<LabeledStroke> classifier, boolean isMiddle) {

        if (isMiddle) {
            // Fit model for middleBag
            for (Graph graph : bag.graphs()) {

                // Sort strokes according to position
                Collections.sort(graph.strokes());

                // TODO: update this in brain as well
                // Get ratio
                int maxDimension = 0;
                for (Stroke s: graph.strokes()) {
                    if (s.box().width() > maxDimension)
                        maxDimension = s.box().width();
                    if (s.box().height() > maxDimension)
                        maxDimension = s.box().height();
                }
                double ratio = 200.0/maxDimension;
                // Get center
                BoundingBox bbox = graph.box();
                Point center = bbox.center;
                // resize
                for (Stroke s: graph.strokes()) {
                    s.resize(ratio, ratio, center);
                }

                for (Stroke s: graph.strokes()) {
                    s.resample(2);
                    s.smoothen(3);
                    s.initLinearity();
                }

                // Get tallest Stroke
                Stroke tallestStroke = Collections.max(graph.strokes(), new Comparator<Stroke>() {
                    @Override
                    public int compare(Stroke me, Stroke other) {
                        return me.box().height() - other.box().height();
                    }
                });

                if (graph.label().equals("\u0915") || graph.label().equals("\u092B"))
                    splitKaFa(graph.strokes(), tallestStroke.box().height());

                // Remove verticals from middle yard
                Tuple<Integer, Integer> removals = removeVerticals(graph.strokes(), tallestStroke.box().height());
                int preVerticals = removals.first;
                int postVerticals= removals.second;

                // if removed more than one verticals, change label to half character
                if (postVerticals > 0)
                    graph.label(graph.label()+Ucode.h.value);

                try {
                    Stroke connected = Stroke.connect(graph.strokes());
                    connected.sanitize();
                    LabeledStroke merged = new LabeledStroke(connected, graph.label());
                    classifier.addExample(merged);
                } catch (ListEmptyError ls) {
                    System.out.println("ERROR: Couldn't connect for middleyard training");
                }
            }
            // Train middle classifier
            classifier.train();
        } else {

            // Fit model for non-middle bag
            for (Graph graph : bag.graphs()) {
                try {
                    Stroke connected = Stroke.connect(graph.strokes());
                    connected.sanitize();
                    LabeledStroke merged = new LabeledStroke(connected, graph.label());
                    classifier.addExample(merged);
                } catch (ListEmptyError ls) {
                    System.out.println(
                            "ERROR: Couldn't connect for non-middle yard training");
                }
            }
            // Train lower classifier
            classifier.train();
        }
    }

    // identifies the strokes and return the breakdown while processing
    public static List<Stroke> workFor(Graph unknown,
            LearningModel<LabeledStroke> upperClassifier,
            LearningModel<LabeledStroke> middleClassifier,
            LearningModel<LabeledStroke> lowerClassifier,
            List<String> suggestionHolder) {
        System.out.println();

        // Copy the input graph, don't destroy the graph while working on it
        Graph graph = unknown.clone();
        List<Stroke> strokes = graph.strokes();

        // TODO: update this in brain as well
        // Get ratio
        int maxDimension = 0;
        for (Stroke s: graph.strokes()) {
            if (s.box().width() > maxDimension)
                maxDimension = s.box().width();
            if (s.box().height() > maxDimension)
                maxDimension = s.box().height();
        }
        double ratio = 200.0/maxDimension;
        // Get center
        BoundingBox bbox = graph.box();
        Point centor = bbox.center;
        // resize
        for (Stroke s: graph.strokes()) {
            s.resize(ratio, ratio, centor);
        }

        // Sanitize before any operation on data
        for (Stroke s: strokes) {
            s.resample(2);
            s.smoothen(3);
            s.initLinearity();
        }

        // For debugging
        List<Stroke> zoneStrokes = new ArrayList<Stroke>();

        // identify
        // String middleLabel="";
        List<String> upperLabel=new ArrayList<String>();
        List<String> lowerLabel=new ArrayList<String>();

        // Split hybrid modifiers
        splitHybridModifiers(strokes);

        try {
            // get internal segments
            Tuple<List<Graph>, Integer> returnVal = Segmentator.internalSegment(strokes);
            List<Graph> graphs = returnVal.first;
            int middleIndex = returnVal.second;

            // get yards
            List<Stroke> upperYard = null;
            List<Stroke> lowerYard = null;
            List<Stroke> middleYard = graphs.get(middleIndex).strokes();
            if (middleIndex-1 >= 0)
                upperYard = graphs.get(middleIndex-1).strokes();
            if (middleIndex+1 < graphs.size())
                lowerYard = graphs.get(middleIndex+1).strokes();

            // Get tallest Stroke
            Stroke tallestStroke = Collections.max(middleYard, new Comparator<Stroke>() {
                @Override
                public int compare(Stroke me, Stroke other) {
                    return me.box().height() - other.box().height();
                }
            });

            // split ka and fa
            splitKaFa(middleYard, tallestStroke.box().height());

            // remove verticals from middle zone
            Tuple<Integer, Integer> removals = removeVerticals(middleYard, tallestStroke.box().height());
            int preVerticals = removals.first;
            int postVerticals= removals.second;

            // VERBOSE:
            System.out.print("Brain.workFor: ");
            System.out.print("Pre "+preVerticals);
            System.out.println("\tPost "+postVerticals);

            // NOTE:
            // the santize() method doesn't change the boundingbox information

            // For middleyard
            Stroke connected = Stroke.connect(middleYard);
            Point center = connected.box().center.clone();
            LabeledStroke middleYardStroke = new LabeledStroke(connected.sanitize(), graph.label());
            List<String> suggestions = middleClassifier.classifyAndSuggest(middleYardStroke);
            middleYardStroke.move(center.x, center.y);
            zoneStrokes.add(middleYardStroke);

            // For upperyard
            if (upperYard != null) {
                for (Stroke s: upperYard) {

                    // FIXME: Find sirbindu
                    if (s.points().size() < 5) {
                        upperLabel.add(Ucode.SI.value);
                        continue;
                    }

                    Point upperCenter = s.box().center.clone();
                    LabeledStroke upperYardStroke = new LabeledStroke(s.sanitize());
                    upperLabel.add(upperClassifier.classify(upperYardStroke));
                    upperYardStroke.move(upperCenter.x, upperCenter.y);
                    zoneStrokes.add(upperYardStroke);
                }
            }
            // For loweryard
            if (lowerYard != null) {
                for (Stroke s: lowerYard) {

                    // for halanta
                    if (s.isNear(135, 20)) {
                        lowerLabel.add(Ucode.rr.value);
                        continue;
                    }

                    Point lowerCenter = s.box().center.clone();
                    LabeledStroke lowerYardStroke = new LabeledStroke(s.sanitize());
                    lowerLabel.add(lowerClassifier.classify(lowerYardStroke));
                    lowerYardStroke.move(lowerCenter.x,lowerCenter.y);
                    zoneStrokes.add(lowerYardStroke);
                }
            }

            if (suggestionHolder==null)
                suggestionHolder = new ArrayList<String>();
            for (String middleLabel: suggestions) {
                // Aggregate the output from different classifiers
                String predictedLabel = Aggregator.aggregate(upperLabel,
                        middleLabel, lowerLabel, preVerticals, postVerticals);
                // Also add label in the graph
                suggestionHolder.add(predictedLabel);
            }
            unknown.label(suggestionHolder.get(0));

        } catch (ListEmptyError ls) {
            System.out.println("ERROR: Some error in workFor");
        }

        return zoneStrokes;
    }

    // splitting ka and fa using a vertical
    // kafa must be 0.6 of this height
    public static void splitKaFa(List<Stroke> strokes, int height) {

        List<Stroke> verticals = new ArrayList<Stroke>();
        List<Stroke> nonverticals = new ArrayList<Stroke>();
        for (Stroke s: strokes) {
            if (s.isNear(90, 20) && s.box().height() > 0.6*height)
                verticals.add(s);
            else if (!s.isLinear())
                nonverticals.add(s);
        }

        for (Stroke l: verticals) {
            for (Stroke nl: nonverticals) {
                // should be overlapped
                if (l.box().overlapArea(nl.box()) > 0) {

                    // NOTE: resampling is required if no. of points is to be considered
                    // as splitting point
                    // nl.resample(3);

                    // NOTE: not needed when balancing is done
                    // but put it for security purposes
                    int aa = Math.abs(l.box().center.x-nl.box().topLeft.x);
                    int bb = Math.abs(nl.box().bottomRight.x-l.box().center.x);
                    double ratio = 1.0*Math.min(aa, bb)/Math.max(aa+1, bb+1);
                    System.out.println("Splitkafa ratio: "+ratio+" must be above 0.33");
                    if (ratio < 0.33)
                        continue;

                    List<Stroke> splittedStrokes = nl.splitVertical(l.box().center.x);
                    if (splittedStrokes.size() <= 1)
                        continue;

                    int a = splittedStrokes.get(0).points().size();
                    int b = splittedStrokes.get(1).points().size();

                    // Fix for ba, balance two sides
                    int c = 0;
                    for (int i=0; i<a && nl.points().get(i).x > l.box().center.x; i++,c=i);
                    b = Math.abs(b-c);

                    System.out.println(a+" "+b);
                    System.out.println(c);

                    // VERBOSE
                    System.out.println("splitKaFa split ratio: "+1.0*Math.min(a,b)/Math.max(a, b)+" must be above 0.20");
                    if (1.0*Math.min(a, b)/ Math.max(a, b) <= 0.20)
                        continue;
                    // VERBOSE:
                    System.out.println("SUCCESS: Splitted kafa");

                    int indexLinear = strokes.indexOf(l);
                    // add second part after vertical and first part before vertical
                    strokes.add(indexLinear+1, splittedStrokes.get(1));
                    strokes.add(indexLinear, splittedStrokes.get(0));
                    strokes.remove(nl);

                    // only split one character
                    break;
                }
            }
        }
        // System.out.println("FAIL: Couldn't split kafa");
    }

    private static boolean splitWithLinear(List<Stroke> inputStrokes) {

        // Sort according according to height in descending order
        Collections.sort(inputStrokes);

        // Stroke linear = inputStrokes.get(0);
        Stroke linear = Collections.max(inputStrokes, new Comparator<Stroke>() {
            @Override
            public int compare(Stroke s, Stroke t) {
                return t.box().center.x - s.box().center.x;
            }
        });

        // First stroke must be linear stroke
        if (!linear.isNear(90, 20)) {
            System.out.println("FAIL: First stroke isn't linear");
            return false;
        }

        for (ListIterator<Stroke> iterator = inputStrokes.listIterator(); iterator.hasNext();) {
            // split if possible and return
            Stroke nonlinear = iterator.next();

            if (nonlinear.isLinear())
                continue;

            // the linear and nonlinear must be overlapping in vertical space
            if (linear.box().vSpace(nonlinear.box()) > 0)
                continue;

            List<Stroke> splittedStrokes = nonlinear.splitSmart(linear);
            if (splittedStrokes == null)
                continue;

            if (splittedStrokes.size() <= 2) {
                System.out.println("FAIL: Wasn't splitted into three parts");
                continue;
            }

            // in split linear, the modifier must go from left to right
            Stroke linearPart = splittedStrokes.get(0);
            Stroke modifierPart  = splittedStrokes.get(1);
            Stroke nonlinearPart = splittedStrokes.get(2);
            if (modifierPart.firstPoint().x >= modifierPart.lastPoint().x) {
                System.out.println("FAIL: Modifier must go from left to right");
                continue;
            }

            System.out.println("SUCCESS: Linear splitted");
            iterator.remove();
            for (Stroke stroke: splittedStrokes)
                iterator.add(stroke);
            inputStrokes.remove(linear);
            return true;
        }
        // System.out.println("FAIL: Couldn't linear split");
        return false;
    }

    private static boolean splitWithoutLinear(List<Stroke> inputStrokes) {

        // Sort according according to height in descending order
        Collections.sort(inputStrokes, new Comparator<Stroke>() {
            @Override
            public int compare(Stroke s, Stroke t) {
                return t.box().height() - s.box().height();
            }
        });
        Stroke tallestStroke = inputStrokes.get(0);
        // Stroke tallestStroke = Stroke.tallest(inputStrokes);

        for (ListIterator<Stroke> iterator = inputStrokes.listIterator(); iterator.hasNext();) {

            Stroke graphStroke = iterator.next();
            if (graphStroke.isLinear())
                continue;

            boolean reversed = false;

            // NOTE: First try from the side with lower y co-ordinate
            if (graphStroke.firstPoint().y < graphStroke.lastPoint().y) {
                graphStroke.reverse();
                reversed = !reversed;
                System.out.println("STATUS: Pre-reversed for splitting");
            }

            // Try first side
            List<Stroke> splittedStrokes = graphStroke.splitSmart(null);

            // a graph with single stroke should be broken into 3 pieces
            if (splittedStrokes != null && splittedStrokes.size()==2 && inputStrokes.size()==1) {
                if (reversed)
                    graphStroke.reverse();
                return false;
            }

            if (splittedStrokes != null && (graphStroke==tallestStroke ||
                        (splittedStrokes.get(1).box().center.y < tallestStroke.box().topLeft.y &&
                         splittedStrokes.get(0).box().center.y > tallestStroke.box().topLeft.y))) {

                iterator.remove();
                // TODO: set modifier in upperYard, set the boundary accordingly
                for (Stroke stroke: splittedStrokes)
                    iterator.add(stroke);
                System.out.println("SUCCESS: Splitted without linear");
                return true;
            } else {
                if (splittedStrokes != null)
                    System.out.println("WARNING: Splitted strokes not consistent with tallestStroke");
            }

            // Try second side
            System.out.println("STATUS: Trying reversal");
            graphStroke.reverse();
            reversed = !reversed;

            splittedStrokes = graphStroke.splitSmart(null);

            // a graph with single stroke should be broken into 3 pieces
            if (splittedStrokes != null && splittedStrokes.size()==2 && inputStrokes.size()==1) {
                if (reversed)
                    graphStroke.reverse();
                return false;
            }

            if (splittedStrokes != null && (graphStroke==tallestStroke ||
                        (splittedStrokes.get(1).box().center.y < tallestStroke.box().topLeft.y &&
                         splittedStrokes.get(0).box().center.y > tallestStroke.box().topLeft.y))) {

                iterator.remove();
                for (Stroke stroke: splittedStrokes)
                    iterator.add(stroke);

                System.out.println("SUCCESS: Splitted without linear");
                return true;
            } else {
                if (splittedStrokes != null)
                    System.out.println("WARNING: Splitted strokes not consistent with tallestStroke");
            }

            System.out.println("FAIL: split without linear failed");
            // if nothing was successful
            if (reversed) {
                System.out.println("STATUS: RE-reversed");
                graphStroke.reverse();
            }
        }
        return false;
    }

    public static void splitHybridModifiers(List<Stroke> inputStrokes) {
        if (splitWithLinear(inputStrokes))
            return;
        splitWithoutLinear(inputStrokes);
    }


    //  remove the vertical lines from the middleYard
    public static Tuple<Integer, Integer> removeVerticals(List<Stroke> strokes, int height) {
        // Identify first stroke that is not linear
        int start = 0;
        while (start < strokes.size() && strokes.get(start).isNear(90, 20)) {
            if (strokes.get(start).box().height() < 0.6*height) {
                System.out.println("ERROR: height of pre vertical is small");
                break;
            }
            if (start+1 < strokes.size()) {
                // NOTE: not needed when balancing is done
                BoundingBox aa = strokes.get(start).box();
                BoundingBox bb = strokes.get(start+1).box();
                if (1.0*Math.abs(aa.center.x - bb.center.x)/bb.width() < 0.3) {
                    System.out.println("ERROR: Prevertical is inside another stroke");
                    break;
                }
            }
            start++;
        }

        // Identify last stroke that is not linear
        int end = strokes.size()-1;
        while (end >= 0 && strokes.get(end).isNear(90, 20)) {
            // NEW FEATURE
            // don't affect kha, sa
            if (strokes.size()==2 && strokes.get(end).box().height() < 0.5*height) {
                System.out.println("ERROR: height of post vertical is small");
                break;
            }
            if (end-1 >= 0) {
                // NOTE: not needed when balancing is done
                BoundingBox aa = strokes.get(end).box();
                BoundingBox bb = strokes.get(end-1).box();
                if (1.0*Math.abs(aa.center.x - bb.center.x)/bb.width() < 0.3) {
                    System.out.println("ERROR: Postvertical is inside another stroke");
                    break;
                }
            }
            end--;
        }

        // if there are only straight strokes, take all strokes
        if (end < start) {
            start = 0;
            end = strokes.size()-1;

            // VERBOSE:
            System.out.println("WARNING: Only vertical lines detected.");
        }

        int preVerticals = start-0;
        int postVerticals = strokes.size()-1-end;

        // remove all pre and post linear strokes
        strokes.subList(end+1, strokes.size()).clear();
        strokes.subList(0, start).clear();

        return new Tuple<Integer, Integer>(preVerticals, postVerticals);
    }
}
