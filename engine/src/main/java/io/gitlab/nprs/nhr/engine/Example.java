package io.gitlab.nprs.nhr.engine;

import java.util.List;

/* interface Example
 * Represents a single training example composed of an input sequence
 * (which is a sequence of 'Point's here) and a string label.
 */
public interface Example {

    public String label();

    public void label(String label);

    public List<Point> input();

    public double[] featureVect();

}
