package io.gitlab.nprs.nhr.engine;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

import org.encog.ml.svm.SVM;
import org.encog.ml.svm.PersistSVM;
import org.encog.ml.svm.SVMType;
import org.encog.ml.svm.KernelType;
import org.encog.ml.svm.training.SVMTrain;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;


/* class SVM
 * Implements the SVM learning model
 */
public class SVMEnc<S extends Example> extends LearningModel<S> {

    private List<S> trainingSet;
    private List<S> testSet;
    private List<S> entireSet;
    private HashMap<String,List<S>> classifiedSet;
    private List<String> classes;
    private SVM svm;
    // ANN model for 'cheating'
    private ANNEnc<S> ann;


    public SVMEnc() {
        trainingSet = new ArrayList<S>();
        testSet = new ArrayList<S>();
        entireSet = new ArrayList<S>();
        classifiedSet = new HashMap<String,List<S>>();
    }

    /* Load a trained model from a file
     */
    public void load(InputStream is) throws IOException {
        ObjectInputStream ois = new ObjectInputStream(is);
        try {
            classes = (ArrayList<String>)ois.readObject();
            System.out.println("Read 'classes' with size "+classes.size());
        } catch (ClassNotFoundException cnfex) {
            System.out.println("CNF exception "+cnfex.getMessage());
            System.exit(1);
        }
        PersistSVM persistor = new PersistSVM();
        svm = (SVM)persistor.read(is);
    }


    /* Add a training example to the model, before
     * training.
     */
    public void addExample(S data) {
        entireSet.add(data);
    }


    /* Used all the added training examples to train the
     * model.
     */
    public void train() {
        System.out.println("SVMEnc.train(): Creating class list");
        classes = new ArrayList<String>();
        // Read examples and create a list of classes
        for (S example : entireSet) {
            if (!classes.contains(example.label())) {
                classes.add(example.label());
            }
        }
        System.out.println("SVMEnc.train(): Class count = "+classes.size());

        S example;
        svm = new SVM(28,SVMType.SupportVectorClassification,
                KernelType.Linear);
        MLDataSet dataSet = new BasicMLDataSet();
        System.out.println("SVMEnc.train(): Populating dataSet with training data");
        for (int i=0; i<entireSet.size(); i++) {
            example = entireSet.get(i);
            //System.out.println("SVMEnc.train(): Label "+example.label());
            MLData inp = new BasicMLData(example.featureVect());
            MLData ideal = new BasicMLData(1);
            ideal.setData(0,(double)classes.indexOf(example.label()));
            dataSet.add(inp,ideal);
        }

        System.out.println("SVMEnc.train(): Training SVM");
        final SVMTrain train = new SVMTrain(svm,dataSet);
        train.iteration();
        train.finishTraining();
    }


    /* Save the trained model to a given file.
     */
    public void save(OutputStream os) throws IOException { 
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(classes);
        PersistSVM persistor = new PersistSVM();
        persistor.save(os,svm);
    }


    /* Classify the input using the model.
     */
    public String classify(S input, boolean silent) { 
        double[] fvect = input.featureVect();
        final MLData output = svm.compute(new BasicMLData(fvect));
        double[] outputData = output.getData();
        return classes.get((int)outputData[0]);
    }

    public String classify(S input) {
        return classify(input,false);
    }

    public void setANN(ANNEnc<S> ann) {
        this.ann = ann;
    }

    public List<String> classifyAndSuggest(S input) {
        // SVMs can't produce suggestions, so let's cheat.
        String svmOP = classify(input);
        List<String> suggestions = new ArrayList<String>();
        suggestions.add(svmOP);
        // TODO eliminate predicted label from suggestion list
        if (ann!=null)
            suggestions.addAll(ann.classifyAndSuggest(input));
        return suggestions;
    }
}
