package io.gitlab.nprs.nhr.engine;

public class ListEmptyError extends Exception {

    public String msg;

    public ListEmptyError(String s) {
        msg = "ListEmptyError at "+s;
    }

}
