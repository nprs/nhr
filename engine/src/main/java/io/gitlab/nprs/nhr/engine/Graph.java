package io.gitlab.nprs.nhr.engine;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Graph {
    protected List<Stroke> lst;

    private String label;

    // constructor
    public Graph(List<Stroke> strokes, String label) throws ListEmptyError {
        if (strokes.isEmpty())
            throw new ListEmptyError("Graph.Graph");

        this.label = label;

        lst = new ArrayList<Stroke>();
        for (Stroke stroke: strokes)
            lst.add(stroke);
    }

    // constructor
    public Graph(List<Stroke> strokes) throws ListEmptyError {
        this(strokes, "?");
    }

    public Graph clone() {
        return new Graph(this);
    }

    // copy constructor
    private Graph(Graph graph) {
        label = graph.label;

        lst = new ArrayList<Stroke>();
        for (Stroke stroke: graph.strokes())
            lst.add(stroke.clone());
    }

    public List<Stroke> strokes() {
        return lst;
    }

    // Convert to a string representation
    public String toString() {
        String graphstr = label + ";";
        for (Stroke stroke : lst)
            graphstr += stroke.toString() + ";";
        return graphstr;
    }

    // FIXME: Potential error here
    public static Graph fromString(String rep) throws ListEmptyError {
        String[] strdata = rep.split(";");
        List<Stroke> strokearr = new ArrayList<Stroke>();
        for (int i=1; i<strdata.length; i++)
            strokearr.add(Stroke.fromString(strdata[i]));

        return new Graph(strokearr, strdata[0]);
    }

    // get label
    public String label() {
        return label;
    }

    // set label
    public void label(String label) {
        this.label = label;
    }

    // return the index of stroke which is linear and near certain angle
    public Stroke hasNear(float angle, float tolerance) {
        for (Stroke stroke: lst) {
            if (stroke.isNear(angle, tolerance))
                return stroke;
        }
        return null;
    }


    public Stroke hasNearTallest(float angle, float tolerance) {
        List<Stroke> outputList = new ArrayList<Stroke>();
        for (Stroke stroke: lst) {
            if (stroke.isNear(angle, tolerance))
                outputList.add(stroke);
        }
        if (outputList.isEmpty())
            return null;
        Collections.sort(outputList, new Comparator<Stroke>() {
            @Override
            public int compare(Stroke a, Stroke b) {
                return - (a.box().height() - b.box().height());
            }
        });
        return outputList.get(0);
    }

    // return the count of stroke which is near given angle
    public int countNear(float angle, float tolerance) {
        int linearCount = 0;
        for (Stroke stroke: lst) {
            if (stroke.isNear(angle, tolerance))
                linearCount += 1;
        }
        return linearCount;
    }

    public void move(int x, int y) {
        BoundingBox bbox = lst.get(0).box().clone();
        for (int i=1; i<lst.size(); i++) {
            bbox.merge(lst.get(i).box());
        }
        int offsetx = x-bbox.center.x;
        int offsety = y-bbox.center.y;
        shift(offsetx, offsety);
    }

    public void shift(int offsetx, int offsety) {
        for (Stroke stroke: lst)
            stroke.shift(offsetx, offsety);
    }

    public BoundingBox box() {
        BoundingBox bbox = lst.get(0).bbox.clone();
        for (int i=1; i<lst.size(); i++) {
            bbox.merge(lst.get(i).bbox);
        }
        return bbox;
    }

}
