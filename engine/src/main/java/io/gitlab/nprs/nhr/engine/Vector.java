package io.gitlab.nprs.nhr.engine;

public class Vector {

    public int x;
    public int y;

    // constructor
    // a position vector of point
    public Vector(Point point) {
        x = point.x;
        y = point.y;
    }

    // constructor
    // from point a to b
    public Vector(Point a, Point b) {
        x = b.x-a.x;
        y = b.y-a.y;
    }

    // copy constructor
    public Vector(Vector v) {
        x = v.x;
        y = v.y;
    }

    // get magnitude of point
    public double magnitude() {
        return Math.pow(x*x+y*y,0.5);
    }

    // get dot product
    public int dotProduct(Vector v){
        return x*v.x + y*v.y;
    }

    // get cosine
    public double cosine(Vector v){
        double denominator = this.magnitude()*v.magnitude();
        if (denominator <= 0.0001)
            return 0;

        double cos = this.dotProduct(v)/denominator;
        // NOTE: problem arises if magnitude of cosine is greater than 1
        if (cos > 1)
            return 1;
        else if (cos < -1)
            return -1;
        return cos;
    }


    public double similarity(Vector v) {
        return Math.abs(cosine(v));
    }

    public double dissimilarity(Vector v) {
        return 1-similarity(v);
    }

}
