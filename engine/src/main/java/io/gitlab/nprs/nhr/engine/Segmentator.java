package io.gitlab.nprs.nhr.engine;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.Collections;

public class Segmentator {

    public static int hSpace = 20;

    // segment in one dimension
    public static int segment(int a, int b, double r) {
        return (int)Math.round((1-r)*a + r*b);
    }

    public static List<Graph> segment(List<Stroke> inputStrokes, List<BoundingBox> boxes) throws ListEmptyError {

        if (inputStrokes.isEmpty())
            throw new ListEmptyError("Segmentator.segment");


        List<Stroke> strokes = new ArrayList<Stroke>();
        for (Stroke s: inputStrokes)
            strokes.add(s);

        // Just sort it so that order is preserved
        Collections.sort(strokes);

        // List of all the segmented graphs
        List<Graph> graphs = new ArrayList<Graph>();
        // List of bounding box
        if (boxes==null)
            boxes = new ArrayList<BoundingBox>();

        for (Stroke stroke: strokes) {
            // create new boundingbox and graph
            BoundingBox newBox = stroke.box().clone();
            ArrayList<Stroke> newGroup = new ArrayList<Stroke>();

            // get all the mergeable boxes because of the new stroke
            List<BoundingBox> mergeableBoxes = new ArrayList<BoundingBox>();
            for (BoundingBox b: boxes) {
                if (b.hSpace(stroke.box()) < hSpace)
                    mergeableBoxes.add(b);
            }

            // remove all mergeables and merge into new one just created
            if (!mergeableBoxes.isEmpty()) {
                for (BoundingBox mergeableBox: mergeableBoxes) {
                    int index = boxes.indexOf(mergeableBox);
                    newBox.merge(mergeableBox);
                    newGroup.addAll(graphs.get(index).strokes());
                    graphs.remove(graphs.get(index));
                    boxes.remove(mergeableBox);
                }
            }

            newGroup.add(stroke);

            // add new boundingbox and group to the list
            boxes.add(newBox);
            graphs.add(new Graph(newGroup));
        }

        // merge single vertical graph to the previous graph
        ListIterator<Graph> iterator = graphs.listIterator();
        Graph previous = iterator.next();
        for (;iterator.hasNext();) {
            Graph current = iterator.next();
            if (current.strokes().size() > 1 ||
                    !current.strokes().get(0).isNear(90, 20)) {
                previous = current;
                continue;
            }

            // add the linear to previous graph
            previous.strokes().add(current.strokes().get(0));
            int index = graphs.indexOf(current);
            // merge the bounding box
            boxes.get(index-1).merge(boxes.get(index));
            // remove one bounding box
            boxes.remove(index);
            // remove the linear
            iterator.remove();

            // no need to update current
        }

        return graphs;
    }

    // move kra from middleYard to lowerYard
        private static void moveKra(List<Graph> graphs, int middleIndex) {
            Graph graph = graphs.get(middleIndex);
            // No need to find kra for a single stroke graph
            if (graph.strokes().size()<=1)
                return;

            // Sort yard, for kra indentification
            Collections.sort(graph.strokes());

            // identify kra and send it to next yard
            Stroke kra = graph.hasNear(135, 20);
            if (kra==null) {
                System.out.println("FAIL: No kra identified");
                return;
            }

            // Kra shouldn't be above the center
            BoundingBox graphBox = graph.box();
            BoundingBox halfBox = new BoundingBox(graphBox.topLeft,
                    new Point(graphBox.bottomRight.x, graphBox.center.y));
            double kraOverlapRatio = kra.box().vOverlapRatio(halfBox);
            System.out.println("Kra Overlap ratio: "+kraOverlapRatio+" must be less than 0.5");
            if (kra.box().vOverlapRatio(halfBox) > 0.5) {
                System.out.println("FAIL: Kra is above the center");
                return;
            }

            System.out.println("SUCCESS: Identified kra");

            // Move kra to lower yard
            if (middleIndex+1 < graphs.size()) {
                // add kra to next yard
                System.out.println("STATUS: Moved kra to existing lowerYard");
                graphs.get(middleIndex+1).strokes().add(kra);
            } else {
                try {
                    System.out.println("STATUS: Moved kra to new lowerYard");
                    List<Stroke> newGroup = new ArrayList<Stroke>();
                    newGroup.add(kra);
                    Graph newGraph = new Graph(newGroup);
                    graphs.add(newGraph);
                } catch (ListEmptyError ls) {
                    // NOTE: should never happen
                    System.out.println(ls.msg);
                }
            }

            // remove only one kra
            graph.strokes().remove(kra);
        }

        private static void mergeForTra(List<Stroke> strokes) {
            // Normal sort
            Collections.sort(strokes);
            // merge adjacent "\ before /" strokes
            for (int i=1; i<strokes.size(); i++) {
                Stroke current = strokes.get(i);
                Stroke old = strokes.get(i-1);
                // if this isn't the "/" stroke and previous isn't the "\" stroke
                boolean currentIsSlant = current.isNear(135, 20);
                boolean oldIsLinear = old.isLinear();
                if (!currentIsSlant || !oldIsLinear) {
                    if (!currentIsSlant)
                        System.out.println("WARNING: Current is not slant for kra");
                    if (!oldIsLinear)
                        System.out.println("WARNING: Old is not linear for kra");
                    continue;
                }
                // if there is a stroke before "\" and there is overlap between
                // then skip merge with "/"
                if (i-2 >= 0) {
                    Stroke older = strokes.get(i-2);
                    double overlapRatio = old.box().vOverlapRatio(older.box());
                    if (overlapRatio > 0.3) {
                        System.out.println("WARNING: Overlap between older and old");
                        continue;
                    }
                }
                // remove both strokes "\" and "/"
                strokes.remove(i);
                strokes.remove(i-1);
                // add the merged stroke
                try {
                    List<Stroke> kras = new ArrayList<Stroke>();
                    kras.add(old);
                    kras.add(current);
                    Stroke kra = Stroke.connect(kras);
                    strokes.add(kra);
                } catch (ListEmptyError ls) {
                    // NOTE: should never happen
                    System.out.println("ERROR: Problem in creating tra");
                }

                System.out.println("SUCCESS: Identified Kra");
                // Only one merge in a character
                break;
            }
        }

        private static List<Graph> separateIntoYards(List<Stroke> strokes) {

            // Sort according to center's y for yard separation
            Collections.sort(strokes, new Comparator<Stroke>() {
                @Override
                public int compare(Stroke s, Stroke t) {
                    return s.box().center.y - t.box().center.y;
                }
            });

            // List of all the segmented graphs
            List<Graph> graphs = new ArrayList<Graph>();
            // List of bounding box
            List<BoundingBox> boxes = new ArrayList<BoundingBox>();

            // Separate into different yards
            for (Stroke stroke: strokes) {
                // create new boundingbox and graph
                BoundingBox newBox = stroke.box().clone();
                // get all the mergeable boxes because of the new stroke
                List<BoundingBox> mergeableBoxes = new ArrayList<BoundingBox>();
                for (BoundingBox b: boxes) {
                    // center inside other
                    System.out.println("Segment Overlap Ratio: "+stroke.box().vOverlapRatio(b)+" must be above 0.55");
                    if ((stroke.box().vOverlapRatio(b) >= 0.55) ||
                            (stroke.box().center.y > b.topLeft.y && stroke.box().center.y < b.bottomRight.y) ||
                            (b.center.y > stroke.box().topLeft.y && b.center.y < stroke.box().bottomRight.y)) {
                        mergeableBoxes.add(b);
                    }
                }

                ArrayList<Stroke> newGroup = new ArrayList<Stroke>();
                // add current stroke to new group
                newGroup.add(stroke);
                // remove all mergeables and merge into new one just created
                if (!mergeableBoxes.isEmpty()) {
                    for (BoundingBox mergeableBox: mergeableBoxes) {
                        // Get graph associated with current bounding box
                        Graph oldGraph = graphs.get(boxes.indexOf(mergeableBox));
                        newBox.merge(mergeableBox);
                        // add all the older strokes to new group
                        newGroup.addAll(oldGraph.strokes());
                        // remove the old graph
                        graphs.remove(oldGraph);
                        // remove the old bounding box
                        boxes.remove(mergeableBox);
                    }
                }

                try {
                    // add new boundingbox
                    boxes.add(newBox);
                    // add new graph
                    graphs.add(new Graph(newGroup));
                } catch (ListEmptyError ls) {
                    // NOTE: should never happen
               }
            }

            return graphs;
        }

    private static int removeShirorekha(List<Graph> graphs) {
        int middleIndex = -1;

        Stroke finalHorizontal = null;
        Graph finalGraph = null;

        // NOTE: must be sorted
        for (Graph g: graphs)
            Collections.sort(g.strokes());

        // Remove Shirorekha
        for (ListIterator<Graph> iterator = graphs.listIterator(); iterator.hasNext();) {

            int graphIndex = iterator.nextIndex();
            Graph graph = iterator.next();


            Stroke horizontal = graph.hasNear(0, 20);

            // if horizontal doesn't exists, skip
            if (horizontal == null)
                continue;

            // if a horizontal exits alone, remove the graph
            if (graph.strokes().size()==1) {

                // add horizontal to loweryard if loweryard exists
                if (iterator.hasNext()) {

                    iterator.remove();

                    Graph nextGraph = iterator.next();
                    if (nextGraph.strokes().size()>1 || nextGraph.hasNear(0, 20)==null) {

                        nextGraph.strokes().add(0, horizontal);
                        // current position will be the middleIndex
                        middleIndex = graphIndex;
                        System.out.println("graphIndex: "+graphIndex);
                        System.out.println("STATUS: Identified shirorekhna alone");

                        iterator.previous();
                    } else {
                        iterator.previous();
                        iterator.add(graph);
                    }
                    // NOTE: don't break
                }

                continue;
            }

            // if horizontal exists with other strokes as well
            // Create bounding box of non-horizontals in graph
            graph.strokes().remove(horizontal);
            BoundingBox bbox = graph.box();
            graph.strokes().add(horizontal);

            double overlapRatio = horizontal.box().hOverlapRatio(bbox);
            System.out.println("shirorekha overlap ratio: "+overlapRatio+" must be above 0.4");
            if (overlapRatio < 0.4)
                continue;

            // Compare with horizontals
            System.out.println("graphIndex: "+graphIndex);


            //FIXME
            if (horizontal.box().center.y < Segmentator.segment(bbox.topLeft.y, bbox.bottomRight.y, 0.3)) {
                middleIndex = graphIndex;
                graph.strokes().remove(horizontal);
                System.out.println("SUCCESS: Identified shirorekha above middle");
                return middleIndex;
            } else if (horizontal.box().center.y > Segmentator.segment(bbox.topLeft.y, bbox.bottomRight.y, 0.7) &&
                    iterator.hasNext()) {
                middleIndex = graphIndex+1;
                graph.strokes().remove(horizontal);
                System.out.println("SUCCES: Identified shirorekha below middle");
                return middleIndex;
            } else {
                System.out.println("STATUS: Not a shirorekha");
            }

        }

        return middleIndex;
    }

    // identify single verticals and information on middle yard
    private static int middleIndexFromVerticals(List<Graph> graphs) {
        int middleIndex = -1;
        for (Graph graph: graphs) {
            boolean singleVertical  = graph.strokes().size()==1 &&
                                      graph.strokes().get(0).isNear(90, 20);
            if (!singleVertical)
                continue;

            System.out.println("SUCCESS: Identified middleyard from verticals");
            int graphIndex = graphs.indexOf(graph);
            // if singleVertical graph is the last graph, middleIndex is
            // the one above it else it is the one lower it
            if (graphIndex == graphs.size()-1)
                middleIndex = graphIndex-1;
            else
                middleIndex = graphIndex+1;
            break;
        }
        return middleIndex;
    }

    private static int middleIndexFromHeight(List<Graph> graphs) {
        int middleIndex = -1;
        // set yard with max height as middle yard
        int tallestHeight = 0;
        for (Graph graph: graphs) {
            for (Stroke stroke: graph.strokes()) {
                if (stroke.box().height() >= tallestHeight) {
                    tallestHeight = stroke.box().height();
                    middleIndex = graphs.indexOf(graph);
                }
            }
        }
        return middleIndex;
    }

    private static int mergeYards(List<Graph> graphs, int middleIndex) {
        // merging all the yards above and below middle yard
        List<Graph> newGraphs = new ArrayList<Graph>();
        List<Stroke> newStrokes = null;
        // merge uppers
        try {
            newStrokes = new ArrayList<Stroke>();
            for (int i=0; i<middleIndex;i++)
                newStrokes.addAll(graphs.get(i).strokes());

            newGraphs.add(new Graph(newStrokes));
        } catch (ListEmptyError ls) {
            System.out.println("STATUS: No upper modifiers");
        }
        // add middle
        newGraphs.add(graphs.get(middleIndex));
        // merge lowers
        try {
            newStrokes = new ArrayList<Stroke>();
            for (int i=middleIndex+1; i<graphs.size();i++)
                newStrokes.addAll(graphs.get(i).strokes());
            newGraphs.add(new Graph(newStrokes));
        } catch (ListEmptyError ls) {
            System.out.println("STATUS: No lower modifiers");
        }
        // set the merged graphs as output graphs
        middleIndex = newGraphs.indexOf(graphs.get(middleIndex));

        // make the changes reflect
        graphs.clear();
        graphs.addAll(newGraphs);

        return middleIndex;
    }


    private static void breakMiddleYard(List<Graph> graphs, int middleIndex) {
        // Re-distribute strokes into lowerYard according to lower edge
        // of the vertical line
        Graph middleYard = graphs.get(middleIndex);
        Stroke vertical = middleYard.hasNearTallest(90, 20);
        if (vertical == null) {
            System.out.println("FAIL: No vertical in middleYard for breaking");
            return;
        }

        // NEW FEATURE - Use tallest linear
        // Remove horizontal using linear line feature
        Stroke horizontal = middleYard.hasNear(0, 20);
        if (horizontal != null && horizontal.box().center.y <
                segment(vertical.box().topLeft.y, vertical.box().bottomRight.y, 0.2)) {
            System.out.println("Removing shirorekha using linear information");
            middleYard.strokes().remove(horizontal);
        }

        int nonVerticalCount = middleYard.strokes().size() - middleYard.countNear(90, 20);
        if (nonVerticalCount <= 1) {
            System.out.println("FAIL: Not enough nonVertical in middleYard for breaking");
            return;
        }

        // get the loweryard if it exists
        Graph lowerYard = null;
        if (middleIndex+1 < graphs.size())
            lowerYard = graphs.get(middleIndex+1);

        for (ListIterator<Stroke> iterator = middleYard.strokes().listIterator(); iterator.hasNext();) {
            Stroke s = iterator.next();
            // check if stroke is below the lower yard
            if (s.box().center.y < vertical.box().bottomRight.y)
                continue;
            // check if there is overlap in horizontal space
            if (s.box().hSpace(vertical.box()) > 0)
                continue;

            if (lowerYard != null) {
                // add to loweryard
                System.out.println("STATUS: Added to existing loweryard");
                lowerYard.strokes().add(s);
            } else {
                // create loweryard
                try {
                System.out.println("STATUS: Added to new loweryard");
                List<Stroke> newGroup = new ArrayList<Stroke>();
                newGroup.add(s);
                lowerYard = new Graph(newGroup);
                graphs.add(lowerYard);
                } catch (ListEmptyError ls) {
                    // NOTE: should never happen
                    System.out.println("ERROR: couldn't move to loweryard");
                }
            }
            // remove stroke below the vertical
            iterator.remove();
        }
    }

    public static Tuple<List<Graph>, Integer> internalSegment(List<Stroke> strokes) throws ListEmptyError {

        if (strokes.isEmpty())
            throw new ListEmptyError("Segmentator.segment");

        mergeForTra(strokes);

        List<Graph> graphs = separateIntoYards(strokes);

        // VERBOSE:
        System.out.print("Yards: ");
        for (Graph graph: graphs)
            System.out.print(graph.strokes().size()+", ");
        System.out.println("");

        // FIXME:
        // Remove zone with only verticals (more than one)
        for (ListIterator<Graph> iterator = graphs.listIterator(); iterator.hasNext();) {
            Graph g = iterator.next();
            if (g.strokes().size() >= 2 && g.countNear(90, 20)>=g.strokes().size()) {
                iterator.remove();
                System.out.println("STATUS: Removed zone with only verticals");
            }
        }

        int middleIndex = removeShirorekha(graphs);
        // if middleIndex is yet not identified
        if (middleIndex < 0)
            middleIndex = middleIndexFromVerticals(graphs);

        // Identify middleIndex if not yet identified
        if (middleIndex > -1)
            System.out.println("STATUS: Identified middle yard");
        else if (graphs.size() == 0)
            middleIndex = -1;
        else if (graphs.size() == 1)
            middleIndex = 0;
        else {
            // if two yards and second has a vertical then, it is middle
            boolean secondGraphHasLinear = graphs.size()==2 &&
                                           graphs.get(1).hasNear(90, 20) != null;
            if (secondGraphHasLinear) {
                middleIndex = 1;
                System.out.println("STATUS: Identified middleIndex from vertical in second graph");
            } else {
                middleIndex = middleIndexFromHeight(graphs);
                System.out.println("STATUS: Identified middleIndex from height");
            }
        }

        moveKra(graphs, middleIndex);

        middleIndex = mergeYards(graphs, middleIndex);

        breakMiddleYard(graphs, middleIndex);

        // VERBOSE:
        for (Graph graph: graphs)
            System.out.print(graph.strokes().size()+" ");
        System.out.print("Middle one: "+(middleIndex+1));
        System.out.println("/"+graphs.size());
        System.out.println();

        return new Tuple<List<Graph>, Integer>(graphs, middleIndex);
    }

}
