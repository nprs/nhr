package io.gitlab.nprs.nhr.engine;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


// NOTE: workFor populates the trainingSet and distances

public class KNN<S extends Example> extends LearningModel<S> {

    private int k;
    private Metric<S> metric;
    private List<S> trainingSet;
    private List<S> entireSet;
    private List<String> classes;
    private HashMap<String,List<S>> classifiedSet;
    private List<Double> distances;
    private List<String> labels;
    private S prevClassified;

    public void load(InputStream is) throws IOException {
    }

    public void save(OutputStream os) throws IOException {
    }
    
    public KNN(int k, Metric<S> metric) {
        this.k = k;
        trainingSet = new ArrayList<S>();
        testSet = new ArrayList<S>();
        entireSet = new ArrayList<S>();
        classifiedSet = new HashMap<String,List<S>>();

        distances = new ArrayList<Double>();
        labels = new ArrayList<String>();

        this.metric = metric;
    }

    public void addExample(S data) {
        trainingSet.add(data);
    }

    public void workFor(S unknown) {
        // get all the trainingSet
        prevClassified = unknown;
        distances = new ArrayList<Double>();
        labels = new ArrayList<String>();
        for (S neighbor: trainingSet) {
            double distance = metric.distance(neighbor, unknown);
            distances.add(distance);
            labels.add(neighbor.label());
        }
        respectiveSort(false, distances, distances, labels);

        // Get top k values
        HashMap<String, Double> weights = new HashMap<String, Double>();
        for (int i=0; i < Math.min(labels.size(), k); i++) {
            Double val = weights.get(labels.get(i));
            // val is null when reading initially
            if (val == null)
                val = 0.0;

            weights.put(labels.get(i), val + 1.0/distances.get(i));
        }

        // Sort this now
        distances = new ArrayList<Double>();
        labels = new ArrayList<String>();
        for (java.util.Map.Entry<String, Double> es: weights.entrySet()) {
            labels.add(es.getKey());
            distances.add(es.getValue());
        }
        respectiveSort(true, distances, distances, labels);

        /*for(int i=0; i<Math.min(k, labels.size()); i++)
            System.out.println(labels.get(i)+" "+distances.get(i));
            */
    }
    
    public void train() {
    }

    public String classify(S input) {
        if (prevClassified==input)
            return predict();
        else {
            workFor(input);
            return predict();
        }
    }

    public List<String> classifyAndSuggest(S input) {
        if (prevClassified==input)
            return predictMore();
        else {
            workFor(input);
            return predictMore();
        }
    }

    public String predict() {
        return labels.get(0);
        // return trainingSet.get(0).label();
    }

    public List<String> predictMore() {
        return labels;
    }

    /*
    public List<Tuple<String, Double>> predictMore() {
        List<Tuple<String, Double>> values = new ArrayList<Tuple<String, Double>>();
        for(int i=0; i<labels.size(); i++) {
            Tuple<String, Double> value = new Tuple<String, Double>(labels.get(i), distances.get(i));
            values.add(value);
        }
        return values;
    }
    */

    public List<String> nearestNeighbors() {
        return labels.subList(0, k);
        // return trainingSet.subList(0, k);
    }

    public List<Double> distances() {
        return distances.subList(0, k);
    }

    // TODO At least one other class uses it, so move it to a better place
    public static <T extends Comparable<T>>
    void respectiveSort(boolean descending, final List<T> key, List<?>... lists){
        // Create a List of indices
        List<Integer> indices = new ArrayList<Integer>();
        for(int i = 0; i < key.size(); i++)
            indices.add(i);

        // Sort the indices list based on the key
        if (descending) {
            Collections.sort(indices, new Comparator<Integer>(){
                @Override public int compare(Integer i, Integer j) {
                    return key.get(j).compareTo(key.get(i));
                }
            });
        } else {
            Collections.sort(indices, new Comparator<Integer>(){
                @Override public int compare(Integer i, Integer j) {
                    return key.get(i).compareTo(key.get(j));
                }
            });
        }

        // Create a mapping that allows sorting of the List by N swaps.
        // Only swaps can be used since we do not know the type of the lists
        Map<Integer,Integer> swapMap = new HashMap<Integer, Integer>(indices.size());
        List<Integer> swapFrom = new ArrayList<Integer>(indices.size()),
            swapTo   = new ArrayList<Integer>(indices.size());
        for(int i = 0; i < key.size(); i++){
            int k = indices.get(i);
            while(i != k && swapMap.containsKey(k))
                k = swapMap.get(k);

            swapFrom.add(i);
            swapTo.add(k);
            swapMap.put(i, k);
        }

        // use the swap order to sort each list by swapping elements
        for(List<?> list : lists)
            for(int i = 0; i < list.size(); i++)
                Collections.swap(list, swapFrom.get(i), swapTo.get(i));
    }
}
