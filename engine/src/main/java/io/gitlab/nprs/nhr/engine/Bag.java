package io.gitlab.nprs.nhr.engine;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

public class Bag {

    private List<Graph> lst;
    private String filename;

    public Bag(String filename) {
        this.filename = filename;
        lst = new ArrayList<Graph>();
    }

    public Bag(String filename, List<Graph> graphs) {
        this.filename = filename;
        lst = new ArrayList<Graph>();
        for(Graph graph: graphs)
            lst.add(graph);
    }

    public void append(Graph graph) {
        lst.add(graph);
    }

    public void remove(Graph graph) {
        lst.remove(graph);
    }

    public void remove(int index) {
        lst.remove(index);
    }

    public int size() {
        return lst.size();
    }

    public Graph elem(int i) {
        return lst.get(i);
    }

    public List<Graph> graphs() {
        return lst;
    }

    // Convert to a string representation
    public String toString() {
        String dictstr = "";
        for (Graph graph : lst)
            dictstr += graph.toString() + "\n";
        return dictstr;
    }

    public static Bag fromString(String filename, String rep) {
        String[] strdata = rep.split("\n");
        List<Graph> grapharr = new ArrayList<Graph>();
        for (int i=0; i<strdata.length; i++) {
            try {
                Graph g = Graph.fromString(strdata[i]);
                grapharr.add(g);
            } catch (ListEmptyError ls) {
                System.out.println(ls.msg);
            }
        }

        return new Bag(filename, grapharr);
    }

    public static Bag load(String filename) {
        Bag dict = new Bag(filename);
        try {
            // open and read from file
            File file = new File(filename);
            FileInputStream fis = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fis.read(data);
            fis.close();

            String text = new String(data, "UTF-8");
            dict = Bag.fromString(filename, text);
            dict.filename = filename;
        } catch (IOException e) {
            System.out.println("Bag.load: Some IO error.");
            System.out.println(e.getMessage());
        }

        // VERBOSE:
        if (!dict.graphs().isEmpty())
            System.out.println("Bag.load: Loaded ("+dict.graphs().size()+") ");

        return dict;
    }

    public void store() {
        File file = new File(this.filename);
        try {
            if (!file.exists())
                file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(this.toString());
            writer.flush();
            writer.close();
        } catch (IOException e){
            System.out.println("Bag.store: Some IO error.");
        }
    }
}
