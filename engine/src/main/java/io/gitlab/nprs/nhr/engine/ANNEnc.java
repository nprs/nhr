package io.gitlab.nprs.nhr.engine;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.persist.EncogDirectoryPersistence;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.engine.network.activation.ActivationSoftMax;

import org.apache.commons.lang3.ArrayUtils;


/* class ANN
 * Implements the ANN learning model
 */
public class ANNEnc<S extends Example> extends LearningModel<S> {

    private List<S> trainingSet;
    private List<S> testSet;
    private List<S> entireSet;
    private HashMap<String,List<S>> classifiedSet;
    private List<String> classes;
    private Map<String,double[]> oneHotClasses;
    private BasicNetwork ann;


    public ANNEnc() {
        entireSet = new ArrayList<S>();
        classifiedSet = new HashMap<String,List<S>>();
        trainingSet = new ArrayList<S>();
        testSet = new ArrayList<S>();
    }

    /* Load a trained model from a file
     */
    public void load(InputStream is) throws IOException {
        ObjectInputStream ois = new ObjectInputStream(is);
        try {
            classes = (ArrayList<String>)ois.readObject();
            System.out.println("Read 'classes' with size "+classes.size());
        } catch (ClassNotFoundException cnfex) {
            System.out.println("CNF exception "+cnfex.getMessage());
            System.exit(1);
        }
        ann = (BasicNetwork)EncogDirectoryPersistence.loadObject(is);
        System.out.println("Read 'ann' with outputCount "+ann.getOutputCount());
    }


    /* Add a training example to the model, before
     * training.
     */
    public void addExample(S data) {
        entireSet.add(data);
    }


    /* Use all the training examples to train the model.
     */
    public void train() {
        System.out.println("ANNEnc.train(): Creating class list");
        classes = new ArrayList<String>();
        // Read examples and create a list of classes
        for (S example : entireSet) {
            if (!classes.contains(example.label()))
                classes.add(example.label());
        }
        System.out.println("ANNEnc.train(): Class count = "+classes.size());

        System.out.println("ANNEnc.train(): Creating one-hot-encoded output vectors");
        oneHotClasses = new HashMap<String,double[]>();
        double[] oneHot;
        int numClasses = classes.size();
        // Associated one-hot-encoded outputs with the labels
        for (int i=0; i<numClasses; i++) {
            oneHot = new double[numClasses];
            for (int j=0; j<numClasses; j++) {
                if (i==j) oneHot[j] = 1.0;
                else oneHot[j] = 0.0;
            }
            oneHotClasses.put(classes.get(i),oneHot);
        }

        // Create network
        ann = new BasicNetwork();
        ann.addLayer(new BasicLayer(null,true,20));
        ann.addLayer(new BasicLayer(new ActivationSigmoid(),true,100));
        ann.addLayer(new BasicLayer(new ActivationSoftMax(),false,numClasses));
        ann.getStructure().finalizeStructure();
        ann.reset();

        // Create training dataSet
        System.out.println("ANNEnc.train(): Populating training dataSet");
        MLDataSet dataSet = new BasicMLDataSet();
        for (int i=0; i<entireSet.size(); i++) {
            S ex = entireSet.get(i);
            MLData inp = new BasicMLData(ex.featureVect());
            MLData ideal = new BasicMLData(oneHotClasses.get(ex.label()));
            dataSet.add(inp,ideal);
        }

        // train the NN
        System.out.println("ANNEnc.train(): Training neural network");
        final ResilientPropagation train = new ResilientPropagation(ann,dataSet);
        int epoch = 1;
        do {
            train.iteration();
            System.out.println("Epoch #"+epoch+" Error:"+train.getError());
            epoch++;
        } while (train.getError()>0.001);
        train.finishTraining();
    }


    /* Save the trained model to a given file.
     */
    public void save(OutputStream os) throws IOException { 
        ObjectOutputStream oos = new ObjectOutputStream(os);
        oos.writeObject(classes);
        EncogDirectoryPersistence.saveObject(os,ann);
    }


    /* Classify the input using the model.
     */
    public String classify(S input, boolean silent) { 
        double[] fvect = input.featureVect();
        final MLData output = ann.compute(new BasicMLData(fvect));
        double[] outputData = output.getData();
        if (!silent) {
            System.out.println("Input vector: (length "+fvect.length+")");
            for (int i=0; i<fvect.length; i++) {
                System.out.println(fvect[i]);
            }
        }
        // Find the most probable class
        int maxIdx = 0;
        double max = 0;
        if (!silent)
            System.out.println("Output vector: (length "+outputData.length+")");
        for (int i=0; i<outputData.length; i++) {
            if (!silent)
                if (outputData[i]>0.001) System.out.println(outputData[i]);
                else System.out.println(0.0);
            if (outputData[i]>max) {
                max = outputData[i];
                maxIdx = i;
            }
        }
        return classes.get(maxIdx);
    }

    public String classify(S input) {
        return classify(input,false);
    }

    public List<String> classifyAndSuggest(S input) {
        List<String> res = new ArrayList<String>(classes);
        /* TEMP
        res.add(classify(input));
        return res;*/
        double[] outputArr = ann.compute(new BasicMLData(input.featureVect())).getData();
        List<Double> outputList = Arrays.asList(ArrayUtils.toObject(outputArr));
        KNN.respectiveSort(true,outputList,res);
        return res;
    }
}
