package io.gitlab.nprs.nhr.engine;

public class Tuple<T, M> {
    public T first;
    public M second;

    Tuple(T t, M m) {
        first = t;
        second = m;
    }
}
