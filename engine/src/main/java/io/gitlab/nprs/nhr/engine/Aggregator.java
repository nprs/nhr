package io.gitlab.nprs.nhr.engine;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class Aggregator {

    public static String aggregate(List<String> u, String m, List<String> l,
                                   int preVerticals, int postVerticals) {

        // if we have a postvertical, then remove halanta if it occurs in middleyard
        if (postVerticals >= 1 && m.contains(Ucode.h.value)) { // 1
            m = m.substring(0, m.length()-1);
            postVerticals -= 1;
        }

        // chandrabindu doesn't contain a sirbindu
        if (u.contains(Ucode.CH.value) && !u.contains(Ucode.SI.value))
            System.out.println("ERROR: Problem in chandrabindu");
        // remove shirbindu if chandrabindu already exists
        if (u.contains(Ucode.SI.value) && u.contains(Ucode.CH.value))
            u.remove(Ucode.SI.value);

        if (preVerticals <= 0 && postVerticals <=0) {
            if (u.contains(Ucode.I.value)) {
                u.remove(Ucode.I.value);
                u.add(Ucode.E.value);
                System.out.println("Changed I to E");
            }
        }

        if (preVerticals >= 1) {
            if (u.contains(Ucode.I.value)) {
                preVerticals -= 1;
            }
        }

        // if it contains E and I
        if (u.contains(Ucode.E.value) && u.contains(Ucode.I.value)) {
            u.remove(Ucode.I.value);
            u.add(Ucode.E.value);
        }

        // if it contains two I
        if (Collections.frequency(u, Ucode.I.value) == 2) {
            u.remove(Ucode.I.value);
            u.remove(Ucode.I.value);
            u.add(0, Ucode.AI.value);
        }

        // convert two ekar into aikar
        if (Collections.frequency(u, Ucode.E.value) == 2) {
            u.remove(Ucode.E.value);
            u.remove(Ucode.E.value);
            u.add(0, Ucode.AI.value);
        }


        if (postVerticals >= 1) {
            // iikar must have a prevertical or postvertical
            if (u.contains(Ucode.I.value)) {
                u.remove(Ucode.I.value);
                u.add(0, Ucode.II.value);
                postVerticals -= 1;
            }
            // convert ekar to okar
            else if (u.contains(Ucode.E.value)) {
                u.remove(Ucode.E.value);
                u.add(0, Ucode.O.value);
                postVerticals -= 1;
            }
            // convert aikar to aukar
            else if (u.contains(Ucode.AI.value)) {
                u.remove(Ucode.AI.value);
                u.add(0, Ucode.AU.value);
                postVerticals -= 1;
            }
            // else add akar
            else {
                u.add(0, Ucode.AA.value);
                postVerticals -= 1;
            }
        }

        if (preVerticals != 0)
            System.out.println("ERROR: Too much pre verticals found.");
        if (postVerticals != 0)
            System.out.println("ERROR: Too much post verticals found.");

        // Merge modifiers
        if (m.equals(Ucode.a.value)) {
            if (u.contains(Ucode.AA.value)) {
                m = Ucode.aa.value;
                u.remove(Ucode.AA.value);
            } else if (u.contains(Ucode.O.value)) {
                m = Ucode.o.value;
                u.remove(Ucode.O.value);
            } else if (u.contains(Ucode.AU.value)) {
                m = Ucode.au.value;
                u.remove(Ucode.AU.value);
            }
        } else if (m.equals(Ucode.i.value)) {
            if (u.contains(Ucode.RR.value)) {
                m = Ucode.ii.value;
                u.remove(Ucode.RR.value);
            }
        } else if (m.equals(Ucode.e.value)) {
            if (u.contains(Ucode.E.value)) {
                m = Ucode.ai.value;
                u.remove(Ucode.E.value);
            }
        }

        // Add adha-ra before the middleyard if "adha-ra" exists in upperyard
        if (u.contains(Ucode.RR.value)) {
            u.remove(Ucode.RR.value);
            m = Ucode.RR.value + m;
        }

        // remove halanta if there is rr
        if (l.contains(Ucode.rr.value) && l.contains(Ucode.h.value))
            l.remove(Ucode.h.value);

        // Add arha+ra after the middleyard if "adha+ra" exists in loweryard
        if (l.contains(Ucode.rr.value)) {
            l.remove(Ucode.rr.value);
            m = m + Ucode.rr.value;
        }


        // if halanta, it should have nothing more, else remove halanta
        // 0. upper shouldn't be more than 4
        // 1. lower shouldn't be more than 2
        // 2. if presence of lower of upper, then it cannot be middle

        // Append upper and lower modifiers to predicted character
        String label= m;
        for (String s: l)
            label += s;
        for (String s: u)
            label += s;

        return label;
    }

}

