package io.gitlab.nprs.nhr.engine;

import java.util.List;

// stroke class with a label used because a unified stroke is used for recognition
public class LabeledStroke extends Stroke implements Example {
    private String label;

    public LabeledStroke(Stroke stroke, String label) {
        super(stroke);
        this.label = label;
    }

    public LabeledStroke(Stroke stroke) {
        super(stroke);
        this.label = "?";
    }

    public String label() {
        return label;
    }

    public void label(String label) {
        this.label = label;
    }

    public List<Point> input() {
        return lst;
    }
}

