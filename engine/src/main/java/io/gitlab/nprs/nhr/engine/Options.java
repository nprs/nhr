package io.gitlab.nprs.nhr.engine;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.HelpFormatter;

// TODO This class should conceptually be outside engine

/* class Options
 * Holder Structure for options and configuration settings for
 * the application.
 */
public class Options {
    // Running mode of the application
    public enum Mode {TRAIN, RECOGNIZE, VIEW, LEARN, VALIDATE, UNKNOWN};
    // Learning model in use by the application
    public enum Model {KNN, SVM, ANN, UNKNOWN};

    public Mode mode;
    public Model model;
    public String dataFile;
    public boolean testOnly; // TODO remove. use model evaluation mode instead.
    public boolean argsDeformed;

    private org.apache.commons.cli.Options opt;
    private CommandLine cmdLine;

    public Options(String[] cmdargs) {
        opt = new org.apache.commons.cli.Options();
        opt.addOption("f","file",true,"Data file");
        opt.addOption("m","mode",true,"Mode of operation [train|recognize|view|learn]");
        opt.addOption("M","model",true,"Learning model to use [SVM|KNN|ANN]");
        opt.addOption("t","testonly",false,"Test only, don't take input");

        CommandLineParser parser = new DefaultParser();
        try {
            cmdLine = parser.parse(opt,cmdargs);
        } catch (ParseException ex) {
            System.out.println("Exception occured while parsing options");
            System.out.println(ex.getMessage());
            System.exit(1);
        }

        mode = Options.parseMode(cmdLine);
        model = Options.parseModel(cmdLine);
        testOnly = cmdLine.hasOption('t');
        if (mode==Mode.UNKNOWN || model==Model.UNKNOWN || !cmdLine.hasOption('f')) {
            argsDeformed = true;
            dataFile = "";
        } else
            dataFile = cmdLine.getOptionValue('f');
    }

    // read the 'mode' input argument
    static Mode parseMode(CommandLine cmd) {
        if (!cmd.hasOption('m'))
            return Mode.RECOGNIZE;

        String modeStr = cmd.getOptionValue('m');
        if (modeStr.equals("train"))
            return Mode.TRAIN;
        else if (modeStr.equals("recognize"))
            return Mode.RECOGNIZE;
        else if (modeStr.equals("view"))
            return Mode.VIEW;
        else if (modeStr.equals("learn"))
            return Mode.LEARN;
        else if (modeStr.equals("validate"))
            return Mode.VALIDATE;
        else
            return Mode.UNKNOWN;
    }

    // read the model input argument
    static Model parseModel(CommandLine cmd) {
        if (!cmd.hasOption('M'))
            return Model.ANN;

        String modeStr = cmd.getOptionValue('M');
        if (modeStr.equals("SVM"))
            return Model.SVM;
        else if (modeStr.equals("KNN"))
            return Model.KNN;
        else if (modeStr.equals("ANN"))
            return Model.ANN;
        else
            return Model.UNKNOWN;
    }

    // print the help string
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("nhr",opt,true);
    }
}
