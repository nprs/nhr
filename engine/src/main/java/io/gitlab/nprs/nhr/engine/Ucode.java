package io.gitlab.nprs.nhr.engine; 

// Nepali Unicode
public enum Ucode{
    h("\u094D"),
    AA("\u093E"), E("\u0947"), AI("\u0948"),
    O("\u094B"), AU("\u094C"), I("\u093F"), II("\u0940"),
    RR("\u0930\u094D"), rr("\u094D\u0930"),
    a("\u0905"), i("\u0907"), e("\u090F"),
    aa("\u0906"), o("\u0913"), au("\u0914"), ii("\u0908"), ai("\u0910"),
    SI("\u0902"), CH("\u0901");

    public String value;

    private Ucode(String value){
        this.value=value;
    }
}

