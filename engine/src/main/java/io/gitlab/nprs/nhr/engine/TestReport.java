package io.gitlab.nprs.nhr.engine;

/* Everything that needs to be reported from tests goes here
 */
public class TestReport {
    public int total;
    public int misclassified;
}
